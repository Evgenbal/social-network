# Yet Another Social Network

**Functionality:**

+ registration 
+ ajax loading of friends  
+ authentication 
+ private messages with friends with webSockets
+ add/delete friends
+ ajax search with pagination
+ live search with ajax and jQuery autocomplete
+ display profile  
+ edit profile  
+ edit profile privacy settings  
+ upload and download avatar  
+ wall posts
+ users export to xml  

**Tools:**  
JDK 8
 Spring 5 Boot/Security/Data/Web/Test/Rest/Websocket,  
 Stomp/SockJs,  
 JPA2 / Hibernate 5,    
 Servlets 3,  
 MySql/ H2,  
 Servlets 3,  
 JUnit 4/ Mockito,   
 Integration tests,   
 Slf4j / Logback,  
 html, css, Bootstrap 3, jsp / jstl, jQuery 2/ ajax, webjars, websocket  
 Maven 3, Git / Bitbucket, Tomcat 8, IntelliJIDEA 17.  
   

**Notes:**  
SQL ddl is located in the `dao/src/main/resources/create_tables/`

**Link to heroku:**


[http://young-waters-91168.herokuapp.com](http://young-waters-91168.herokuapp.com)

First login: mainMail@mail.ru   
Second login: mainMailSecond@mail.ru    
Password: 1234

Java WEB, MAY 2017 

--  
**Balakin Evgeny**  
Тренинг getJavaJob,   
[http://www.getjavajob.com](http://www.getjavajob.com)
