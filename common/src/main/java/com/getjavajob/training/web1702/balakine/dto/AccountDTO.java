package com.getjavajob.training.web1702.balakine.dto;

public class AccountDTO {
    private long accountId;

    private String firstName;

    private String secondName;

    private String photo;

    protected AccountDTO() {

    }


    public long getAccountId() {
        return accountId;
    }

    public void setAccountId(long accountId) {
        this.accountId = accountId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }


    @Override
    public String toString() {
        return "AccountDTO{" +
                "accountId=" + accountId +
                ", firstName='" + firstName + '\'' +
                ", secondName='" + secondName + '\'' +
                '}';
    }

    public static final class AccountDTOBuilder {
        private long accountId;
        private String firstName;
        private String secondName;
        private String photo;

        private AccountDTOBuilder() {
        }

        public static AccountDTOBuilder anAccountDTO() {
            return new AccountDTOBuilder();
        }

        public AccountDTOBuilder withAccountId(long accountId) {
            this.accountId = accountId;
            return this;
        }

        public AccountDTOBuilder withFirstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public AccountDTOBuilder withSecondName(String secondName) {
            this.secondName = secondName;
            return this;
        }

        public AccountDTOBuilder withPhoto(String photo) {
            this.photo = photo;
            return this;
        }

        public AccountDTO build() {
            AccountDTO accountDTO = new AccountDTO();
            accountDTO.setAccountId(accountId);
            accountDTO.setFirstName(firstName);
            accountDTO.setSecondName(secondName);
            accountDTO.setPhoto(photo);
            return accountDTO;
        }
    }
}
