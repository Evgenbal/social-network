package com.getjavajob.training.web1702.balakine.dto;

public class ChatChannelDTO {


    private String channelUuid;

    private AccountDTO firstAccount;

    private AccountDTO secondAccount;

    protected ChatChannelDTO() {
    }

    public String getChannelUuid() {
        return channelUuid;
    }

    public void setChannelUuid(String channelUuid) {
        this.channelUuid = channelUuid;
    }

    public AccountDTO getFirstAccount() {
        return firstAccount;
    }

    public void setFirstAccount(AccountDTO firstAccount) {
        this.firstAccount = firstAccount;
    }

    public AccountDTO getSecondAccount() {
        return secondAccount;
    }

    public void setSecondAccount(AccountDTO secondAccount) {
        this.secondAccount = secondAccount;
    }

    @Override
    public String toString() {
        return "ChatChannelDTO{" +
                "channelUuid='" + channelUuid + '\'' +
                ", firstAccount=" + firstAccount +
                ", secondAccount=" + secondAccount +
                '}';
    }

    public static final class ChatChannelDTOBuilder {
        private String channelUuid;
        private AccountDTO firstAccount;
        private AccountDTO secondAccount;

        private ChatChannelDTOBuilder() {
        }

        public static ChatChannelDTOBuilder aChatChannelDTO() {
            return new ChatChannelDTOBuilder();
        }

        public ChatChannelDTOBuilder withChannelUuid(String channelUuid) {
            this.channelUuid = channelUuid;
            return this;
        }

        public ChatChannelDTOBuilder withFirstAccount(AccountDTO firstAccount) {
            this.firstAccount = firstAccount;
            return this;
        }

        public ChatChannelDTOBuilder withSecondAccount(AccountDTO secondAccount) {
            this.secondAccount = secondAccount;
            return this;
        }

        public ChatChannelDTO build() {
            ChatChannelDTO chatChannelDTO = new ChatChannelDTO();
            chatChannelDTO.setChannelUuid(channelUuid);
            chatChannelDTO.setFirstAccount(firstAccount);
            chatChannelDTO.setSecondAccount(secondAccount);
            return chatChannelDTO;
        }
    }
}
