package com.getjavajob.training.web1702.balakine.dto;


import java.util.Date;

public class ChatMessageDTO {

    private long senderId;

    private long recipientId;

    private String text;

    private Date timeOfSend;



    protected ChatMessageDTO() {
    }

    public long getSenderId() {
        return senderId;
    }

    public void setSenderId(long senderId) {
        this.senderId = senderId;
    }


    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getTimeOfSend() {
        return timeOfSend;
    }

    public void setTimeOfSend(Date timeOfSend) {
        this.timeOfSend = timeOfSend;
    }

    public long getRecipientId() {
        return recipientId;
    }

    public void setRecipientId(long recipientId) {
        this.recipientId = recipientId;
    }

    @Override
    public String toString() {
        return "ChatMessageDTO{" +
                "senderId=" + senderId +
                ", recipientId=" + recipientId +
                ", text='" + text + '\'' +
                ", timeOfSend=" + timeOfSend +
                '}';
    }


    public static final class ChatMessageDTOBuilder {
        private long senderId;
        private long recipientId;
        private String text;
        private Date timeOfSend;

        private ChatMessageDTOBuilder() {
        }

        public static ChatMessageDTOBuilder aChatMessageDTO() {
            return new ChatMessageDTOBuilder();
        }

        public ChatMessageDTOBuilder withSenderId(long senderId) {
            this.senderId = senderId;
            return this;
        }

        public ChatMessageDTOBuilder withRecipientId(long recipientId) {
            this.recipientId = recipientId;
            return this;
        }

        public ChatMessageDTOBuilder withText(String text) {
            this.text = text;
            return this;
        }

        public ChatMessageDTOBuilder withTimeOfSend(Date timeOfSend) {
            this.timeOfSend = timeOfSend;
            return this;
        }

        public ChatMessageDTO build() {
            ChatMessageDTO chatMessageDTO = new ChatMessageDTO();
            chatMessageDTO.setSenderId(senderId);
            chatMessageDTO.setRecipientId(recipientId);
            chatMessageDTO.setText(text);
            chatMessageDTO.setTimeOfSend(timeOfSend);
            return chatMessageDTO;
        }
    }
}
