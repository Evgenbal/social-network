package com.getjavajob.training.web1702.balakine.dto;

import java.util.UUID;

public class InitChatChannelDTO {

    private String uuid;

    private long userOneId;

    private long userTwoId;

    protected InitChatChannelDTO() {
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public long getUserOneId() {
        return userOneId;
    }

    public void setUserOneId(long userOneId) {
        this.userOneId = userOneId;
    }

    public long getUserTwoId() {
        return userTwoId;
    }

    public void setUserTwoId(long userTwoId) {
        this.userTwoId = userTwoId;
    }

    @Override
    public String toString() {
        return "InitChatChannelDTO{" +
                "uuid='" + uuid + '\'' +
                ", userOneId=" + userOneId +
                ", userTwoId=" + userTwoId +
                '}';
    }

    public static final class ChatChannelDtoBuilder {
        private String uuid;
        private long userOneId;
        private long userTwoId;

        private ChatChannelDtoBuilder() {
        }

        public static ChatChannelDtoBuilder aChatChannelDto() {
            return new ChatChannelDtoBuilder();
        }

        public ChatChannelDtoBuilder withChatChannelId(String chatChannelId) {
            this.uuid = chatChannelId;
            return this;
        }

        public ChatChannelDtoBuilder withUserOneId(long userOneId) {
            this.userOneId = userOneId;
            return this;
        }

        public ChatChannelDtoBuilder withUserTwoId(long userTwoId) {
            this.userTwoId = userTwoId;
            return this;
        }

        public InitChatChannelDTO build() {
            InitChatChannelDTO initChatChannelDTO = new InitChatChannelDTO();
            initChatChannelDTO.setUuid(uuid != null ? uuid : UUID.randomUUID().toString());
            initChatChannelDTO.setUserOneId(userOneId);
            initChatChannelDTO.setUserTwoId(userTwoId);
            return initChatChannelDTO;
        }
    }
}
