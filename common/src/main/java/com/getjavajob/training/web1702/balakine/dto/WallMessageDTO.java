package com.getjavajob.training.web1702.balakine.dto;

import java.time.LocalDateTime;

public class WallMessageDTO {

    private long messageId;

    private long senderId;

    private String senderFirstName;

    private String senderSecondName;

    private String senderPhoto;

    private String text;

    private LocalDateTime postTime;


    protected WallMessageDTO() {
    }

    public long getMessageId() {
        return messageId;
    }

    public void setMessageId(long messageId) {
        this.messageId = messageId;
    }

    public long getSenderId() {
        return senderId;
    }

    public void setSenderId(long senderId) {
        this.senderId = senderId;
    }

    public String getSenderFirstName() {
        return senderFirstName;
    }

    public void setSenderFirstName(String senderFirstName) {
        this.senderFirstName = senderFirstName;
    }

    public String getSenderSecondName() {
        return senderSecondName;
    }

    public void setSenderSecondName(String senderSecondName) {
        this.senderSecondName = senderSecondName;
    }

    public String getSenderPhoto() {
        return senderPhoto;
    }

    public void setSenderPhoto(String senderPhoto) {
        this.senderPhoto = senderPhoto;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public LocalDateTime getPostTime() {
        return postTime;
    }

    public void setPostTime(LocalDateTime postTime) {
        this.postTime = postTime;
    }


    @Override
    public String toString() {
        return "WallMessageDTO{" +
                "messageId=" + messageId +
                ", senderId=" + senderId +
                ", senderFirstName='" + senderFirstName + '\'' +
                ", senderSecondName='" + senderSecondName + '\'' +
                ", senderPhoto='" + (senderPhoto != null) + '\'' +
                ", text='" + text + '\'' +
                ", time=" + postTime +
                '}';
    }

    public static final class WallMessageDTOBuilder {
        private long messageId;
        private long senderId;
        private String senderFirstName;
        private String senderSecondName;
        private String senderPhoto;
        private String text;
        private LocalDateTime postTime;

        private WallMessageDTOBuilder() {
        }

        public static WallMessageDTOBuilder aWallMessageDTO() {
            return new WallMessageDTOBuilder();
        }

        public WallMessageDTOBuilder withMessageId(long messageId) {
            this.messageId = messageId;
            return this;
        }

        public WallMessageDTOBuilder withSenderId(long senderId) {
            this.senderId = senderId;
            return this;
        }

        public WallMessageDTOBuilder withSenderFirstName(String senderFirstName) {
            this.senderFirstName = senderFirstName;
            return this;
        }

        public WallMessageDTOBuilder withSenderSecondName(String senderSecondName) {
            this.senderSecondName = senderSecondName;
            return this;
        }

        public WallMessageDTOBuilder withSenderPhoto(String senderPhoto) {
            this.senderPhoto = senderPhoto;
            return this;
        }

        public WallMessageDTOBuilder withText(String text) {
            this.text = text;
            return this;
        }

        public WallMessageDTOBuilder withPostTime(LocalDateTime postTime) {
            this.postTime = postTime;
            return this;
        }

        public WallMessageDTO build() {
            WallMessageDTO wallMessageDTO = new WallMessageDTO();
            wallMessageDTO.setMessageId(messageId);
            wallMessageDTO.setSenderId(senderId);
            wallMessageDTO.setSenderFirstName(senderFirstName);
            wallMessageDTO.setSenderSecondName(senderSecondName);
            wallMessageDTO.setSenderPhoto(senderPhoto);
            wallMessageDTO.setText(text);
            wallMessageDTO.setPostTime(postTime);
            return wallMessageDTO;
        }
    }
}
