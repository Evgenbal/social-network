package com.getjavajob.training.web1702.balakine.model;

import org.springframework.security.core.userdetails.UserDetails;

import java.io.Serializable;

public abstract class AbstractModel implements Serializable{

    public abstract long getId();


}
