package com.getjavajob.training.web1702.balakine.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.getjavajob.training.web1702.balakine.utils.AccountRole;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;


@Entity
@Table(name = "ACCOUNTS_TBL")
public class Account extends AbstractMainModel  {

    private static final Long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private long accountId;


    @Email(message = "Enter valid mail")
    @Size( max = 32)
    private String mail;

    private String password;

    @NotBlank(message = "Please enter your first name")
    @Column(name = "first_name")
    private String firstName;

    @NotBlank(message = "Please enter your second name")
    @Column(name = "second_name")
    private String secondName;

    @Column(name = "third_name")
    private String thirdName;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate birthday;

    @Column(name = "home_address")
    private String homeAddress;

    @Column(name = "work_address")
    private String workAddress;

    private String icq;

    private String skype;


    @OneToMany(
            mappedBy = "user",
            fetch = FetchType.EAGER,
            cascade = CascadeType.ALL,
            orphanRemoval = true,
            targetEntity = com.getjavajob.training.web1702.balakine.model.Phone.class
    )
    @JsonManagedReference
    private List<Phone> phones;


    @Column(name = "date_of_registration")
    private LocalDate dateOfRegistration;


    @Enumerated(EnumType.STRING)
    private AccountRole role;


    @Lob
    @Column(length = 100000)
    private byte[] photo;


    //constructor

    protected  Account() {

    }




    @Override
    public int hashCode() {
        return mail.hashCode();
    }


    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        Account item = (Account) obj;
        if (!this.mail.equals(item.mail)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {

        return "Account{" +
                "id=" + accountId +
                ", mail='" + mail + '\'' +
                ", password='" + password + '\'' +
                ", firstName='" + firstName + '\'' +
                ", secondName='" + secondName + '\'' +
                ", thirdName='" + thirdName + '\'' +
                ", birthday=" + birthday +
                ", phones=" + phones +
                ", homeAddress='" + homeAddress + '\'' +
                ", workAddress='" + workAddress + '\'' +
                ", icq='" + icq + '\'' +
                ", skype='" + skype + '\'' +
                "}\n";
    }

    // Getters & Setters

    @Override
    @Transient
    public long getId() {
        return accountId;
    }


    public long getAccountId() {
        return accountId;
    }

    public void setAccountId(long accountId) {
        this.accountId = accountId;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getThirdName() {
        return thirdName;
    }

    public void setThirdName(String thirdName) {
        this.thirdName = thirdName;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public String getHomeAddress() {
        return homeAddress;
    }

    public void setHomeAddress(String homeAddress) {
        this.homeAddress = homeAddress;
    }

    public String getWorkAddress() {
        return workAddress;
    }

    public void setWorkAddress(String workAddress) {
        this.workAddress = workAddress;
    }

    public String getIcq() {
        return icq;
    }

    public void setIcq(String icq) {
        this.icq = icq;
    }

    public String getSkype() {
        return skype;
    }

    public void setSkype(String skype) {
        this.skype = skype;
    }

    public List<Phone> getPhones() {
        return phones;
    }

    public void setPhones(List<Phone> phones) {
        this.phones = phones;
    }


    public LocalDate getDateOfRegistration() {
        return dateOfRegistration;
    }

    public void setDateOfRegistration(LocalDate dateOfRegistration) {
        this.dateOfRegistration = dateOfRegistration;
    }

    public AccountRole getRole() {
        return role;
    }

    public void setRole(AccountRole role) {
        this.role = role;
    }

    @Override
    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }


    public static final class AccountBuilder {
        private long accountId;
        private String mail;
        private String password;
        private String firstName;
        private String secondName;
        private String thirdName;
        private LocalDate birthday;
        private String homeAddress;
        private String workAddress;
        private String icq;
        private String skype;
        private LocalDate dateOfRegistration;
        private List<Phone> phones;

        private AccountBuilder() {
        }

        public static AccountBuilder anAccount() {
            return new AccountBuilder();
        }


        public AccountBuilder withAccountId(long accountId) {
            this.accountId = accountId;
            return this;
        }

        public AccountBuilder withMail(String mail) {
            this.mail = mail;
            return this;
        }

        public AccountBuilder withPassword(String password) {
            this.password = password;
            return this;
        }

        public AccountBuilder withFirstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public AccountBuilder withSecondName(String secondName) {
            this.secondName = secondName;
            return this;
        }

        public AccountBuilder withThirdName(String thirdName) {
            this.thirdName = thirdName;
            return this;
        }

        public AccountBuilder withBirthday(LocalDate birthday) {
            this.birthday = birthday;
            return this;
        }

        public AccountBuilder withHomeAddress(String homeAddress) {
            this.homeAddress = homeAddress;
            return this;
        }

        public AccountBuilder withWorkAddress(String workAddress) {
            this.workAddress = workAddress;
            return this;
        }

        public AccountBuilder withIcq(String icq) {
            this.icq = icq;
            return this;
        }

        public AccountBuilder withSkype(String skype) {
            this.skype = skype;
            return this;
        }

        public AccountBuilder withPhones(List<Phone> phones) {
            this.phones = phones;
            return this;
        }

        public AccountBuilder withDateOfRegistration(LocalDate dateOfRegistration) {
            this.dateOfRegistration = dateOfRegistration;
            return this;
        }

        public Account build() {
            Account account = new Account();
            account.setAccountId(accountId);
            account.setMail(mail);
            account.setPassword(password);
            account.setFirstName(firstName);
            account.setSecondName(secondName);
            account.setThirdName(thirdName);
            account.setBirthday(birthday);
            account.setHomeAddress(homeAddress);
            account.setWorkAddress(workAddress);
            account.setIcq(icq);
            account.setSkype(skype);
            account.setDateOfRegistration(dateOfRegistration);
            account.setPhones(phones);
            return account;
        }
    }
}
