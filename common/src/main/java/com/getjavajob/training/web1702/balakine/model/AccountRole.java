package com.getjavajob.training.web1702.balakine.model;

import java.io.Serializable;

public enum  AccountRole implements Serializable {
    USER("USER"), ADMIN("ADMIN");

    private String val;

    AccountRole(String val) {
        this.val = val;
    }

    public String getVal() {
        return val;
    }
}
