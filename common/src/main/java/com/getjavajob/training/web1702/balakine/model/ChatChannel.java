package com.getjavajob.training.web1702.balakine.model;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "CHAT_CHANNEL_TBL")
public class ChatChannel {

    @Id
    @Column(name = "chat_channel_id")
    private String uuid;


    @OneToOne
    @JoinColumn(name = "account_id_one")
    private Account userOne;


    @OneToOne
    @JoinColumn(name = "account_id_two")
    private Account userTwo;


    protected ChatChannel() {
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Account getUserOne() {

        return userOne;
    }

    public void setUserOne(Account userOne) {
        this.userOne = userOne;
    }

    public Account getUserTwo() {
        return userTwo;
    }

    public void setUserTwo(Account userTwo) {
        this.userTwo = userTwo;
    }

    @Override
    public String toString() {
        return "ChatChannel{" +
                "uuid='" + uuid + '\'' +
                ", userOne=" + userOne +
                ", userTwo=" + userTwo +
                '}';
    }

    public static final class ChatChannelBuilder {
        private String uuid;
        private Account userOne;
        private Account userTwo;

        private ChatChannelBuilder() {
        }

        public static ChatChannelBuilder aChatChannel() {
            return new ChatChannelBuilder();
        }

        public ChatChannelBuilder withChatChannelId(String chatChannelId) {
            this.uuid = chatChannelId;
            return this;
        }

        public ChatChannelBuilder withUserOne(Account userOne) {
            this.userOne = userOne;
            return this;
        }

        public ChatChannelBuilder withUserTwo(Account userTwo) {
            this.userTwo = userTwo;
            return this;
        }

        public ChatChannel build() {
            ChatChannel chatChannel = new ChatChannel();
            chatChannel.setUuid(uuid == null ? UUID.randomUUID().toString() : uuid);
            chatChannel.setUserOne(userOne);
            chatChannel.setUserTwo(userTwo);
            return chatChannel;
        }
    }
}
