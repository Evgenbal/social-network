package com.getjavajob.training.web1702.balakine.model;


import com.fasterxml.jackson.annotation.JsonBackReference;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.SortedMap;

@Entity
@Table(name = "FRIENDS_TBL")
public class Friends extends AbstractModel {
    private static final Long serialVersionUID = 1L;



    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "friendships_id")
    private long friendshipId;



    @ManyToOne
    @JsonBackReference
    @JoinColumn(name = "friend_requester_id", referencedColumnName = "user_id")
    private Account friendRequester;



    @ManyToOne
    @JsonBackReference
    @JoinColumn(name = "friend_accepter_id", referencedColumnName = "user_id")
    private Account friendAccepter;

    private LocalDateTime date;




    protected Friends() {
    }




    @Override
    public String toString() {
        return "\n"+ friendRequester.getMail() + "; " + friendAccepter.getMail();
    }


    @Override
    @Transient
    public long getId() {
        return friendshipId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Friends friends = (Friends) o;

        if (!friendRequester.equals(friends.friendRequester)) {
            return false;
        }
        return friendAccepter.equals(friends.friendAccepter);
    }

    @Override
    public int hashCode() {

        if (friendAccepter != null) {
            int result = (int) friendRequester.getId();
            result = 31 * result + (int) friendAccepter.getId();
            return result;
        } else {
            return super.hashCode();
        }
    }




    public long getFriendshipId() {
        return friendshipId;
    }

    public void setFriendshipId(long friendshipId) {
        this.friendshipId = friendshipId;
    }


    public Account getFriendRequester() {
        return friendRequester;
    }

    public void setFriendRequester(Account friendRequester) {
        this.friendRequester = friendRequester;
    }


    public Account getFriendAccepter() {
        return friendAccepter;
    }

    public void setFriendAccepter(Account friendAccepter) {
        this.friendAccepter = friendAccepter;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }


    public static final class FriendsBuilder {
        private long friendshipId;
        private Account friendRequester;
        private Account friendAccepter;
        private LocalDateTime date;

        private FriendsBuilder() {
        }

        public static FriendsBuilder aFriends() {
            return new FriendsBuilder();
        }

        public FriendsBuilder withFriendshipId(long friendshipId) {
            this.friendshipId = friendshipId;
            return this;
        }

        public FriendsBuilder withFriendRequester(Account friendRequester) {
            this.friendRequester = friendRequester;
            return this;
        }

        public FriendsBuilder withFriendAccepter(Account friendAccepter) {
            this.friendAccepter = friendAccepter;
            return this;
        }

        public FriendsBuilder withDate(LocalDateTime date) {
            this.date = date;
            return this;
        }

        public Friends build() {
            Friends friends = new Friends();
            friends.setFriendshipId(friendshipId);
            friends.setFriendRequester(friendRequester);
            friends.setFriendAccepter(friendAccepter);
            friends.setDate(date);
            return friends;
        }
    }
}
