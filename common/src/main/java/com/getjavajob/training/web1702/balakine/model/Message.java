package com.getjavajob.training.web1702.balakine.model;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.time.LocalDateTime;


@Entity
@Table(name = "MESSAGES_TBL")
public class Message extends AbstractModel {

    private static final Long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "message_id")

    private long id;


    @ManyToOne
    @JoinColumn(name = "sender_id", referencedColumnName = "user_id")
    private Account sender;

    @ManyToOne
    @JoinColumn(name = "recipient_id", referencedColumnName = "user_id")
    private Account recipient;


    @Column(name = "date_of_message")
    private LocalDateTime dateOfMessage;

    @Column(name = "message_text")
    private String messageText;

    @Column(name = "readed")
    private boolean read;

    protected Message() {
    }

    @Override
    public String toString() {
        return "Message{" +
                "sender=" + sender.getId() +
                ", recipient=" + recipient.getId() +
                ", dateOfMessage=" + dateOfMessage +
                ", messageText='" + messageText + '\'' +
                ", read=" + read +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Message message = (Message) o;

        if (read != message.read) {
            return false;
        }
        if (!sender.equals(message.sender)) {
            return false;
        }
        if (!recipient.equals(message.recipient)) {
            return false;
        }
        return this.messageText.equals(message.messageText);
    }

    @Override
    public int hashCode() {
        int result = sender.hashCode();
        result = 31 * result + recipient.hashCode();
        result = 31 * result + dateOfMessage.hashCode();
        result = 31 * result + messageText.hashCode();
        result = 31 * result + (read ? 1 : 0);
        return result;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Account getSender() {
        return sender;
    }

    public void setSender(Account sender) {
        this.sender = sender;
    }

    public Account getRecipient() {
        return recipient;
    }

    public void setRecipient(Account recipient) {
        this.recipient = recipient;
    }


    public LocalDateTime getDateOfMessage() {
        return dateOfMessage;
    }

    public void setDateOfMessage(LocalDateTime dateOfMessage) {
        this.dateOfMessage = dateOfMessage;
    }

    public String getMessageText() {
        return messageText;
    }

    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }


    public static final class MessageBuilder {

        private long id;
        private Account sender;
        private Account recipient;
        private LocalDateTime dateOfMessage;
        private String messageText;
        private boolean read;

        private MessageBuilder() {
        }

        public static MessageBuilder aMessage() {
            return new MessageBuilder();
        }

        public MessageBuilder withId(long id) {
            this.id = id;
            return this;
        }

        public MessageBuilder withSender(Account sender) {
            this.sender = sender;
            return this;
        }

        public MessageBuilder withRecipient(Account recipient) {
            this.recipient = recipient;
            return this;
        }

        public MessageBuilder withDateOfMessage(LocalDateTime dateOfMessage) {
            this.dateOfMessage = dateOfMessage;
            return this;
        }

        public MessageBuilder withMessage(String message) {
            this.messageText = message;
            return this;
        }

        public MessageBuilder withReaded(boolean readed) {
            this.read = readed;
            return this;
        }

        public Message build() {
            Message message = new Message();
            message.setId(id);
            message.setSender(sender);
            message.setRecipient(recipient);
            message.setDateOfMessage(dateOfMessage);
            message.setMessageText(messageText);
            message.read = this.read;
            return message;
        }
    }
}


