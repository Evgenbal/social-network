package com.getjavajob.training.web1702.balakine.model;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;

@Entity
@Table(name = "PHONES_TBL")
public class Phone extends AbstractModel{

    private static final Long serialVersionUID = 1L;


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "phone_id")
    private long phoneId;


    @ManyToOne
    @JoinColumn(name = "user_id")
    @JsonBackReference
    private Account user;


    @Column(name = "phone_number")
    private String phoneNumber;


    protected Phone() {
    }


    @Override
    public String toString() {
        long userId;
        userId = (user == null) ? 0 : user.getAccountId();
        return "Phone{" +
                "id=" + phoneId +
                ", user=" + userId +
                ", phoneNumber='" + phoneNumber + '\'' +
                '}';
    }

    @Override
    public int hashCode() {
        int prime = 31;
        int result = 1;
        result = result * prime + user.hashCode();
        result = result * phoneNumber.hashCode();
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        Phone item = (Phone) obj;
        if (!this.user.equals(item.user)) {
            return false;
        }
        if (!this.phoneNumber.equals(item.phoneNumber)) {
            return false;
        }
        return true;
    }

    @Override
    @Transient
    public long getId() {
        return phoneId;
    }

    public long getPhoneId() {
        return phoneId;
    }

    public void setPhoneId(long phoneId) {
        this.phoneId = phoneId;
    }

    public Account getUser() {
        return user;
    }

    public void setUser(Account user) {
        this.user = user;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }


    public static final class PhoneBuilder {
        private long phoneId;
        private Account user;
        private String phoneNumber;

        private PhoneBuilder() {
        }

        public static PhoneBuilder aPhone() {
            return new PhoneBuilder();
        }

        public PhoneBuilder withPhoneId(long phoneId) {
            this.phoneId = phoneId;
            return this;
        }

        public PhoneBuilder withUser(Account user) {
            this.user = user;
            return this;
        }

        public PhoneBuilder withPhone(String phone) {
            this.phoneNumber = phone;
            return this;
        }

        public Phone build() {
            Phone phone = new Phone();
            phone.setPhoneId(phoneId);
            phone.setUser(user);
            phone.setPhoneNumber(phoneNumber);
            return phone;
        }
    }
}
