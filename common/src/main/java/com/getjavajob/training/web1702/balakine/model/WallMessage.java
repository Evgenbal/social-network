package com.getjavajob.training.web1702.balakine.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;


@Entity
@Table(name = "WALL_MESSAGES_TBL")
public class WallMessage extends AbstractModel {

    private static final Long serialVersionUID = 1L;


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "wall_message_id")
    private long userWallMessageId;


    @ManyToOne
    @JsonIgnore
    @JoinColumn(name = "owner_id", referencedColumnName = "user_id")
    private Account owner;


    @ManyToOne
    @JsonIgnore
    @JoinColumn(name = "writer_id", referencedColumnName = "user_id")
    private Account writer;

    private String message;

    @Column(name = "date_of_message")
    private LocalDateTime dateOfMessage;

    protected WallMessage() {
    }

    public long getUserWallMessageId() {
        return userWallMessageId;
    }

    public void setUserWallMessageId(long userWallMessageId) {
        this.userWallMessageId = userWallMessageId;
    }


    public Account getOwner() {
        return owner;
    }

    public void setOwner(Account owner) {
        this.owner = owner;
    }


    public Account getWriter() {
        return writer;
    }

    public void setWriter(Account writer) {
        this.writer = writer;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public LocalDateTime getDateOfMessage() {
        return dateOfMessage;
    }

    public void setDateOfMessage(LocalDateTime dateOfMessage) {
        this.dateOfMessage = dateOfMessage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        WallMessage message1 = (WallMessage) o;

        if (owner != null ? !owner.equals(message1.owner) : message1.owner != null) {
            return false;
        }
        if (writer != null ? !writer.equals(message1.writer) : message1.writer != null) {
            return false;
        }
        if (message != null ? !message.equals(message1.message) : message1.message != null) {
            return false;
        }
        return dateOfMessage != null ? dateOfMessage.equals(message1.dateOfMessage) : message1.dateOfMessage == null;
    }

    @Override
    public int hashCode() {
        int result = owner != null ? owner.hashCode() : 0;
        result = 31 * result + (writer != null ? writer.hashCode() : 0);
        result = 31 * result + (message != null ? message.hashCode() : 0);
        result = 31 * result + (dateOfMessage != null ? dateOfMessage.hashCode() : 0);
        return result;
    }

    @Override
    public long getId() {
        return userWallMessageId;
    }


    public static final class WallMessageBuilder {
        private long userWallMessageId;
        //@JsonBackReference
        private Account owner;
        //@JsonBackReference
        private Account writer;
        private String message;
        private LocalDateTime dateOfMessage;

        private WallMessageBuilder() {
        }

        public static WallMessageBuilder aWallMessage() {
            return new WallMessageBuilder();
        }

        public WallMessageBuilder withUserWallMessageId(long userWallMessageId) {
            this.userWallMessageId = userWallMessageId;
            return this;
        }

        public WallMessageBuilder withOwner(Account owner) {
            this.owner = owner;
            return this;
        }

        public WallMessageBuilder withWriter(Account writer) {
            this.writer = writer;
            return this;
        }

        public WallMessageBuilder withMessage(String message) {
            this.message = message;
            return this;
        }

        public WallMessageBuilder withDateOfMessage(LocalDateTime dateOfMessage) {
            this.dateOfMessage = dateOfMessage;
            return this;
        }

        public WallMessage build() {
            WallMessage wallMessage = new WallMessage();
            wallMessage.setUserWallMessageId(userWallMessageId);
            wallMessage.setOwner(owner);
            wallMessage.setWriter(writer);
            wallMessage.setMessage(message);
            wallMessage.setDateOfMessage(dateOfMessage);
            return wallMessage;
        }
    }
}
