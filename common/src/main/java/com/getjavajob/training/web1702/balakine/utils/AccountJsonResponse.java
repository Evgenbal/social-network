package com.getjavajob.training.web1702.balakine.utils;

import com.getjavajob.training.web1702.balakine.model.Account;

import java.util.Map;

public class AccountJsonResponse {
    private Account account;
    private boolean validated;
    private Map<String, String> errors;

    protected AccountJsonResponse() {
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public boolean isValidated() {
        return validated;
    }

    public void setValidated(boolean validated) {
        this.validated = validated;
    }

    public Map<String, String> getErrors() {
        return errors;
    }

    public void setErrors(Map<String, String> errors) {
        this.errors = errors;
    }


    public static final class AccountJsonResponseBuilder {
        private Account account;
        private boolean validated;
        private Map<String, String> errors;

        private AccountJsonResponseBuilder() {
        }

        public static AccountJsonResponseBuilder anAccountJsonResponse() {
            return new AccountJsonResponseBuilder();
        }

        public AccountJsonResponseBuilder withAccount(Account account) {
            this.account = account;
            return this;
        }

        public AccountJsonResponseBuilder withValidated(boolean validated) {
            this.validated = validated;
            return this;
        }

        public AccountJsonResponseBuilder withErrors(Map<String, String> errors) {
            this.errors = errors;
            return this;
        }

        public AccountJsonResponse build() {
            AccountJsonResponse accountJsonResponse = new AccountJsonResponse();
            accountJsonResponse.setAccount(account);
            accountJsonResponse.setValidated(validated);
            accountJsonResponse.setErrors(errors);
            return accountJsonResponse;
        }
    }
}
