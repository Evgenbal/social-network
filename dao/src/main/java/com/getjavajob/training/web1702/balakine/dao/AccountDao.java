package com.getjavajob.training.web1702.balakine.dao;

import com.getjavajob.training.web1702.balakine.model.Account;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AccountDao extends JpaRepository<Account, Long> {

    Account findAccountByMail(String mail);


    @Query("select ac from Account ac where lower( ac.firstName) like lower(concat( '%',?1,'%')) " +
            "or lower(ac.secondName) like lower(concat( '%',?1,'%'))")
    List<Account> findByFilterWithPagination(String filter, Pageable pageable);


    @Query("select ac from Account ac where lower( ac.firstName) like lower(concat( '%',?1,'%')) " +
            "or lower(ac.secondName) like lower(concat( '%',?1,'%'))")
    List<Account> findAllByFilter(String filter);

    @Query("select count(ac) from Account ac where lower( ac.firstName) like lower(concat( '%',?1,'%')) " +
            "or lower(ac.secondName) like lower(concat( '%',?1,'%'))")
    int getCountOfAccountsByFilter(String filter);

}
