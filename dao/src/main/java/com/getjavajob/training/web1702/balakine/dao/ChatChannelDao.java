package com.getjavajob.training.web1702.balakine.dao;

import com.getjavajob.training.web1702.balakine.model.ChatChannel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ChatChannelDao extends JpaRepository<ChatChannel, String> {

    @Query("select cc from ChatChannel cc where cc.userOne.id in (?1, ?2) and cc.userTwo.id in (?1, ?2)")
    List<ChatChannel> getChatChannel(long accountIdOne, long accountIdTwo);

    @Query("select cc.uuid from ChatChannel cc where cc.userOne.id in (?1, ?2) and cc.userTwo.id in (?1, ?2)")
    String getChannelId(long accountIdOne, long accountIdTwo);

}
