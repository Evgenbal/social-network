package com.getjavajob.training.web1702.balakine.dao;

import com.getjavajob.training.web1702.balakine.model.Account;
import com.getjavajob.training.web1702.balakine.model.Friends;
import com.getjavajob.training.web1702.balakine.model.Message;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

public interface FriendsDao extends JpaRepository<Friends, Long> {

    List<Friends> findAllByFriendRequester_AccountId(long friendRequesterId);


    List<Friends> findAllByFriendAccepter_AccountId(long friendAccepterId);

    @Query("select fr from Friends fr where fr.friendRequester.accountId = ?1 and fr.friendAccepter.accountId = ?2")
    Friends findFriendShip(long friendRequesterId , long friendAccepterId);

}
