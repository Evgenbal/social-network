package com.getjavajob.training.web1702.balakine.dao;

import com.getjavajob.training.web1702.balakine.model.Account;
import com.getjavajob.training.web1702.balakine.model.Message;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MessageDao extends JpaRepository<Message, Long> {


    List<Message> getAllBySenderAndRecipient(Account sender, Account recipient);

}
