package com.getjavajob.training.web1702.balakine.dao;

import com.getjavajob.training.web1702.balakine.model.Account;
import com.getjavajob.training.web1702.balakine.model.WallMessage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface WallMessageDao extends JpaRepository<WallMessage, Long> {

    List<WallMessage> findAllByOwner(Account owner);
}
