package com.getjavajob.training.web1702.balakine.dao.config;


import com.getjavajob.training.web1702.balakine.dao.AccountDao;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import javax.sql.DataSource;

@Configuration
@EntityScan(basePackages = {"com.getjavajob.training.web1702.balakine.model"})
@EnableJpaRepositories("com.getjavajob.training.web1702.balakine.dao")
public class DaoConfig {
    @Bean
    @ConfigurationProperties("app.datasource.localhost")
    public DataSourceProperties localhostDatasourceProperties() {
        return new DataSourceProperties();
    }

    @Bean
    @ConfigurationProperties("app.datasource.localhost")
    public DataSource localHostDatasource() {
        return localhostDatasourceProperties().initializeDataSourceBuilder().build();
    }
    @Bean
    @Primary
    @ConfigurationProperties("app.datasource.heroku")
    public DataSourceProperties herokuDatasourceProperties() {
        return new DataSourceProperties();
    }

    @Bean
    @Primary
    @ConfigurationProperties("app.datasource.heroku")
    public DataSource herokuHostDatasource() {
        return herokuDatasourceProperties().initializeDataSourceBuilder().build();
    }

}
