CREATE TABLE `ACCOUNTS_TBL` (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `mail` varchar(30) NOT NULL DEFAULT '',
  `password` varchar(200) NOT NULL DEFAULT '',
  `first_name` varchar(20) NOT NULL DEFAULT '',
  `second_name` varchar(20) NOT NULL DEFAULT '',
  `third_name` varchar(20) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `home_address` varchar(20) DEFAULT NULL,
  `work_address` varchar(20) DEFAULT NULL,
  `icq` varchar(20) DEFAULT NULL,
  `skype` varchar(20) DEFAULT NULL,
  `date_of_registration` date DEFAULT NULL,
  `photo` mediumblob,
  `role` char(5) NOT NULL DEFAULT 'USER',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `email` (`mail`)
) ;