CREATE TABLE `CHAT_CHANNEL_TBL` (
  `chat_channel_id` varchar(200) NOT NULL DEFAULT '',
  `account_id_one` bigint(20) NOT NULL,
  `account_id_two` bigint(20) NOT NULL,
  PRIMARY KEY (`chat_channel_id`)
);