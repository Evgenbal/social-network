CREATE TABLE `FRIENDS_TBL` (
  `friendships_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `friend_requester_id` bigint(20) NOT NULL,
  `friend_accepter_id` bigint(20) NOT NULL,
  `date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`friendships_id`)
) ;