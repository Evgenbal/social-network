CREATE TABLE `MESSAGES_TBL` (
  `message_id` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `sender_id` bigint(20) NOT NULL,
  `recipient_id` bigint(11) NOT NULL,
  `message_text` varchar(2000) NOT NULL DEFAULT '',
  `date_of_message` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `readed` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`message_id`)
)