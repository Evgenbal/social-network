CREATE TABLE `PHONES_TBL` (
  `phone_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `phoneNumber` varchar(50) DEFAULT NULL,
  `user_id` bigint(20) NOT NULL,
  PRIMARY KEY (`phone_id`)
) ;