CREATE TABLE `WALL_MESSAGES_TBL` (
  `wall_message_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `owner_id` bigint(20) NOT NULL,
  `date_of_message` datetime NOT NULL,
  `message` varchar(2000) NOT NULL DEFAULT '',
  `writer_id` bigint(20) NOT NULL,
  PRIMARY KEY (`wall_message_id`)
)