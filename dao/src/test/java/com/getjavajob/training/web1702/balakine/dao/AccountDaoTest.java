package com.getjavajob.training.web1702.balakine.dao;

import com.getjavajob.training.web1702.balakine.dao.config.DaoConfig;
import com.getjavajob.training.web1702.balakine.model.Account;
import com.getjavajob.training.web1702.balakine.model.Phone;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;


@RunWith(SpringJUnit4ClassRunner.class)
@DataJpaTest
@ContextConfiguration(classes = DaoConfig.class)
public class AccountDaoTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private AccountDao accountDao;

    private Account firstAccount;
    private Account secondAccount;
    private Account thirdAccount;


    @Before
    public void init() {

        List<Phone> phonesFirst = new ArrayList<>();
        phonesFirst.add(Phone.PhoneBuilder.aPhone()
                .withPhone("1-1")
                .withUser(firstAccount)
                .build());
        phonesFirst.add(Phone.PhoneBuilder.aPhone()
                .withPhone("1-2")
                .withUser(firstAccount)
                .build());

        List<Phone> phonesSecond = new ArrayList<>();
        phonesFirst.add(Phone.PhoneBuilder.aPhone()
                .withPhone("2-1")
                .withUser(secondAccount)
                .build());
        phonesFirst.add(Phone.PhoneBuilder.aPhone()
                .withPhone("2-2")
                .withUser(secondAccount)
                .build());

        List<Phone> phonesThird = new ArrayList<>();
        phonesFirst.add(Phone.PhoneBuilder.aPhone()
                .withPhone("3-1")
                .withUser(thirdAccount)
                .build());
        phonesFirst.add(Phone.PhoneBuilder.aPhone()
                .withPhone("3-2")
                .withUser(thirdAccount)
                .build());


        firstAccount = Account.AccountBuilder.anAccount()
                .withMail("firstAccount@mail.ru")
                .withPassword("1111")
                .withFirstName("firstAccountFirstName")
                .withSecondName("firstAccountSecondName")
                .withThirdName("firstAccountThirdName")
                .withSkype("skype")
                .withIcq("11111111")
                .withHomeAddress("homeAddress")
                .withWorkAddress("workAddress")
                .withBirthday(LocalDate.parse("1991-04-01"))
                .withDateOfRegistration(LocalDate.parse("2003-01-01"))
                .withPhones(phonesFirst)
                .build();

        secondAccount = Account.AccountBuilder.anAccount()
                .withMail("secondAccount@mail.ru")
                .withPassword("222")
                .withFirstName("secondAccountFirstName")
                .withSecondName("secondAccountSecondName")
                .withThirdName("secondAccountThirdName")
                .withSkype("skype2")
                .withIcq("2222222")
                .withHomeAddress("homeAddress2")
                .withWorkAddress("workAddress2")
                .withBirthday(LocalDate.parse("1991-04-02"))
                .withDateOfRegistration(LocalDate.parse("2003-01-02"))
                .withPhones(phonesSecond)
                .build();

        thirdAccount = Account.AccountBuilder.anAccount()
                .withMail("thirdAccount@mail.ru")
                .withPassword("333")
                .withFirstName("thirdAccountFirstName")
                .withSecondName("thirdAccountSecondName")
                .withThirdName("thirdAccountThirdName")
                .withSkype("skype3")
                .withIcq("33333333333")
                .withHomeAddress("homeAddress3")
                .withWorkAddress("workAddress3")
                .withBirthday(LocalDate.parse("1991-04-03"))
                .withDateOfRegistration(LocalDate.parse("2003-01-03"))
                .withPhones(phonesThird)
                .build();


    }


    @Test
    public void createOk() {
        Account accountSaved = accountDao.saveAndFlush(firstAccount);

        Account account = entityManager.find(Account.class, accountSaved.getId());

        assertEqualsAccountAllFields(firstAccount, account);
    }

    @Test
    public void readById() {
        long id = entityManager.persistAndGetId(firstAccount, Long.class);
        entityManager.flush();

        Account account = accountDao.getOne(id);

        assertEqualsAccountAllFields(firstAccount, account);

    }


    @Test
    public void readByMail() {
        entityManager.persist(firstAccount);
        entityManager.persist(secondAccount);
        entityManager.persist(thirdAccount);
        entityManager.flush();

        System.out.println("find");
        Account found = accountDao.findAccountByMail(secondAccount.getMail());

        System.out.println("found");
        assertEqualsAccountAllFields(secondAccount, found);

    }


    @Test(expected = NullPointerException.class)
    public void readByMailNPE() {
        entityManager.persistAndFlush(firstAccount);

        Account found = accountDao.findAccountByMail("randomMail");

        assertEqualsAccountAllFields(firstAccount, found);


    }

    public void update() {
        entityManager.persistAndFlush(firstAccount);

        firstAccount.setFirstName("NewName");

        accountDao.save(firstAccount);

        Account account = entityManager.find(Account.class, firstAccount.getMail());

        assertEquals(firstAccount.getFirstName(), account.getFirstName());
    }


    @Test
    public void deleteById() {
        long id = entityManager.persistAndGetId(firstAccount, Long.class);
        entityManager.flush();

        accountDao.delete(id);
        Account actual = accountDao.findOne(id);
        assertNull(actual);
    }

    @Test
    public void deleteByObject() {
        entityManager.persistAndFlush(firstAccount);

        accountDao.delete(firstAccount);
        Account actual = accountDao.findAccountByMail(firstAccount.getMail());
        assertNull(actual);
    }


    @Test
    public void findAll() {
        entityManager.persist(firstAccount);
        entityManager.persist(secondAccount);
        entityManager.persist(thirdAccount);
        entityManager.flush();

        List<Account> founded = accountDao.findAll();
        long expectedCount = 3;

        assertEquals(expectedCount, founded.size());

    }

    @Test
    public void searchByWordInFirstMail() {
        Account test = Account.AccountBuilder.anAccount()
                .withMail("test1@mail.ru")
                .withFirstName("test")
                .withSecondName("test")
                .build();
        entityManager.persist(firstAccount);
        entityManager.persist(secondAccount);
        entityManager.persist(thirdAccount);
        entityManager.persist(test);
        entityManager.flush();

        List<Account> expected = new ArrayList<>();
        expected.add(firstAccount);
        expected.add(secondAccount);
        expected.add(thirdAccount);

        List<Account> actual = accountDao.findAllByFilter("name");

        assertEquals(expected, actual);

    }


    @Test
    public void getCountOfFoundedAccounts()  {
        Account test = Account.AccountBuilder.anAccount()
                .withMail("test1@mail.ru")
                .withFirstName("test")
                .withSecondName("test")
                .build();

        entityManager.persist(firstAccount);
        entityManager.persist(secondAccount);
        entityManager.persist(thirdAccount);
        entityManager.persist(test);
        entityManager.flush();

        long expected = 3;
        long actual = accountDao.getCountOfAccountsByFilter("name");
        assertEquals(expected, actual);
    }



    private void assertEqualsAccountAllFields(Account expected, Account actual) {
        assertEquals(expected.getId(), actual.getId());
        assertEquals(expected.getMail(), actual.getMail());
        assertEquals(expected.getPassword(), actual.getPassword());
        assertEquals(expected.getFirstName(), actual.getFirstName());
        assertEquals(expected.getSecondName(), actual.getSecondName());
        assertEquals(expected.getThirdName(), actual.getThirdName());
        assertEquals(expected.getSkype(), actual.getSkype());
        assertEquals(expected.getIcq(), actual.getIcq());
        assertEquals(expected.getHomeAddress(), actual.getHomeAddress());
        assertEquals(expected.getWorkAddress(), actual.getWorkAddress());
        assertEquals(expected.getBirthday(), actual.getBirthday());
        assertEquals(expected.getDateOfRegistration(), actual.getDateOfRegistration());

        assertEquals(expected.getPhones(), actual.getPhones());

    }
}
