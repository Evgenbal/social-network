package com.getjavajob.training.web1702.balakine.dao;


import com.getjavajob.training.web1702.balakine.dao.config.DaoConfig;
import com.getjavajob.training.web1702.balakine.model.Account;
import com.getjavajob.training.web1702.balakine.model.Friends;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
@ContextConfiguration(classes = DaoConfig.class)
public class FriendsDaoTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private FriendsDao friendsDao;

    private Account firstAccount;
    private Account secondAccount;
    private Account thirdAccount;

    private Friends friendsShipFirst;
    private Friends friendsShipSecond;
    private Friends friendsShipThird;
    private Friends friendsShipFourth;

    @Before
    public void init(){
        firstAccount = Account.AccountBuilder.anAccount()
                .withMail("firstAccount@mail.ru")
                .withPassword("1111")
                .withFirstName("firstAccountFirstName")
                .withSecondName("firstAccountSecondName")
                .withThirdName("firstAccountThirdName")
                .withSkype("skype")
                .withIcq("11111111")
                .withHomeAddress("homeAddress")
                .withWorkAddress("workAddress")
                .withBirthday(LocalDate.parse("1991-04-01"))
                .withDateOfRegistration(LocalDate.parse("2003-01-01"))
                .build();

        secondAccount = Account.AccountBuilder.anAccount()
                .withMail("secondAccount@mail.ru")
                .withPassword("222")
                .withFirstName("secondAccountFirstName")
                .withSecondName("secondAccountSecondName")
                .withThirdName("secondAccountThirdName")
                .withSkype("skype2")
                .withIcq("2222222")
                .withHomeAddress("homeAddress2")
                .withWorkAddress("workAddress2")
                .withBirthday(LocalDate.parse("1991-04-02"))
                .withDateOfRegistration(LocalDate.parse("2003-01-02"))
                .build();

        thirdAccount = Account.AccountBuilder.anAccount()
                .withMail("thirdAccount@mail.ru")
                .withPassword("333")
                .withFirstName("thirdAccountFirstName")
                .withSecondName("thirdAccountSecondName")
                .withThirdName("thirdAccountThirdName")
                .withSkype("skype3")
                .withIcq("33333333333")
                .withHomeAddress("homeAddress3")
                .withWorkAddress("workAddress3")
                .withBirthday(LocalDate.parse("1991-04-03"))
                .withDateOfRegistration(LocalDate.parse("2003-01-03"))
                .build();

        entityManager.persist(firstAccount);
        entityManager.persist(secondAccount);
        entityManager.persist(thirdAccount);
        entityManager.flush();

        friendsShipFirst = Friends.FriendsBuilder.aFriends()
                .withFriendRequester(firstAccount)
                .withFriendAccepter(secondAccount)
                .build();

        friendsShipSecond = Friends.FriendsBuilder.aFriends()
                .withFriendRequester(secondAccount)
                .withFriendAccepter(firstAccount)
                .build();

        friendsShipThird = Friends.FriendsBuilder.aFriends()
                .withFriendRequester(firstAccount)
                .withFriendAccepter(thirdAccount)
                .build();

        friendsShipFourth = Friends.FriendsBuilder.aFriends()
                .withFriendRequester(secondAccount)
                .withFriendAccepter(thirdAccount)
                .build();
    }

    @Test
    public void createOk() {
        Friends friendsSaved = friendsDao.saveAndFlush(friendsShipFirst);

        Friends actual = entityManager.find(Friends.class, friendsSaved.getId());

        assertEquals(friendsShipFirst, actual);
        assertNotEquals(friendsShipSecond, actual);
    }

    @Test
    public void readById() {
        long id = entityManager.persistAndGetId(friendsShipFirst, Long.class);
        entityManager.flush();

        Friends actual = friendsDao.getOne(id);

        assertEquals(friendsShipFirst, actual);
        assertNotEquals(friendsShipSecond, actual);
    }

    @Test
    public void readByRequesterAndAccepter() {
        entityManager.persist(friendsShipFirst);
        entityManager.persist(friendsShipSecond);
        entityManager.persist(friendsShipThird);//first and third
        entityManager.persist(friendsShipFourth);
        entityManager.flush();

        Friends found = friendsDao.findFriendShip(firstAccount.getId(), thirdAccount.getId());

        assertEquals(friendsShipThird, found);
        assertNotEquals(friendsShipFirst, found);

    }

    @Test
    public void readByAccepterID() {
        entityManager.persist(friendsShipFirst);
        entityManager.persist(friendsShipSecond);
        entityManager.persist(friendsShipThird);//this
        entityManager.persist(friendsShipFourth);//this
        entityManager.flush();

        List<Friends> expected = new ArrayList<>();
        expected.add(friendsShipThird);
        expected.add(friendsShipFourth);

        List<Friends> found = friendsDao.findAllByFriendAccepter_AccountId(thirdAccount.getId());

        assertEquals(expected, found);

    }

    @Test
    public void readByRequesterID() {
        entityManager.persist(friendsShipFirst);//this
        entityManager.persist(friendsShipSecond);
        entityManager.persist(friendsShipThird);//this
        entityManager.persist(friendsShipFourth);
        entityManager.flush();

        List<Friends> expected = new ArrayList<>();
        expected.add(friendsShipFirst);
        expected.add(friendsShipThird);

        List<Friends> found = friendsDao.findAllByFriendRequester_AccountId(firstAccount.getId());

        assertEquals(expected, found);

    }



    @Test
    public void deleteById() {
        long deletedId = (Long) entityManager.persistAndGetId(friendsShipFirst);
        entityManager.persist(friendsShipSecond);
        entityManager.persist(friendsShipThird);
        entityManager.persist(friendsShipFourth);
        entityManager.flush();

        friendsDao.delete(deletedId);
        Friends actual = friendsDao.findOne(deletedId);

        assertNull(actual);

        long actualCount = friendsDao.count();
        long expectedCount = 3;

        assertEquals(expectedCount, actualCount);
    }

    @Test
    public void deleteByObject() {
        entityManager.persist(friendsShipFirst);
        entityManager.persist(friendsShipSecond);
        entityManager.persist(friendsShipThird);
        entityManager.persist(friendsShipFourth);
        entityManager.flush();

        friendsDao.delete(friendsShipFirst);

        long actualCount = friendsDao.count();
        long expectedCount = 3;

        assertEquals(expectedCount, actualCount);
    }


    @Test
    public void findAll() {
        entityManager.persist(friendsShipFirst);
        entityManager.persist(friendsShipSecond);
        entityManager.persist(friendsShipThird);
        entityManager.persist(friendsShipFourth);
        entityManager.flush();

        List<Friends> founded = friendsDao.findAll();
        long expectedCount = 4;

        assertEquals(expectedCount, founded.size());

    }
}
