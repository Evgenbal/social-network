package com.getjavajob.training.web1702.balakine.dao;

import com.getjavajob.training.web1702.balakine.dao.config.DaoConfig;
import com.getjavajob.training.web1702.balakine.model.Account;
import com.getjavajob.training.web1702.balakine.model.Message;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNull;

@RunWith(SpringRunner.class)
@DataJpaTest
@ContextConfiguration(classes = DaoConfig.class)
public class MessageDaoTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private MessageDao messageDao;

    private Account firstAccount;
    private Account secondAccount;
    private Account thirdAccount;

    private Message fromFirstToSecond;
    private Message fromSecondToFirst;
    private Message fromFirstToThird;
    private Message fromSecondToThird;

    @Before
    public void init(){
        firstAccount = Account.AccountBuilder.anAccount()
                .withMail("firstAccount@mail.ru")
                .withPassword("1111")
                .withFirstName("firstAccountFirstName")
                .withSecondName("firstAccountSecondName")
                .withThirdName("firstAccountThirdName")
                .withSkype("skype")
                .withIcq("11111111")
                .withHomeAddress("homeAddress")
                .withWorkAddress("workAddress")
                .withBirthday(LocalDate.parse("1991-04-01"))
                .withDateOfRegistration(LocalDate.parse("2003-01-01"))
                .build();

        secondAccount = Account.AccountBuilder.anAccount()
                .withMail("secondAccount@mail.ru")
                .withPassword("222")
                .withFirstName("secondAccountFirstName")
                .withSecondName("secondAccountSecondName")
                .withThirdName("secondAccountThirdName")
                .withSkype("skype2")
                .withIcq("2222222")
                .withHomeAddress("homeAddress2")
                .withWorkAddress("workAddress2")
                .withBirthday(LocalDate.parse("1991-04-02"))
                .withDateOfRegistration(LocalDate.parse("2003-01-02"))
                .build();

        thirdAccount = Account.AccountBuilder.anAccount()
                .withMail("thirdAccount@mail.ru")
                .withPassword("333")
                .withFirstName("thirdAccountFirstName")
                .withSecondName("thirdAccountSecondName")
                .withThirdName("thirdAccountThirdName")
                .withSkype("skype3")
                .withIcq("33333333333")
                .withHomeAddress("homeAddress3")
                .withWorkAddress("workAddress3")
                .withBirthday(LocalDate.parse("1991-04-03"))
                .withDateOfRegistration(LocalDate.parse("2003-01-03"))
                .build();

        entityManager.persist(firstAccount);
        entityManager.persist(secondAccount);
        entityManager.persist(thirdAccount);
        entityManager.flush();

        fromFirstToSecond = Message.MessageBuilder.aMessage()
                .withMessage("fromFirstToSecond")
                .withDateOfMessage(LocalDateTime.now())
                .withSender(firstAccount)
                .withRecipient(secondAccount)
                .build();

        fromSecondToFirst = Message.MessageBuilder.aMessage()
                .withMessage("fromSecondToFirst")
                .withSender(secondAccount)
                .withRecipient(firstAccount)
                .build();

        fromFirstToThird = Message.MessageBuilder.aMessage()
                .withMessage("fromFirstToThird")
                .withSender(firstAccount)
                .withRecipient(thirdAccount)
                .build();

        fromSecondToThird = Message.MessageBuilder.aMessage()
                .withMessage("fromSecondToThird")
                .withSender(secondAccount)
                .withRecipient(thirdAccount)
                .build();

    }


    @Test
    @Transactional
    public void createOk() {
        Message savedMessage = messageDao.saveAndFlush(fromFirstToSecond);
        messageDao.save(fromFirstToSecond);

        Message actual = entityManager.find(Message.class, savedMessage.getId());

        assertEquals(fromFirstToSecond, actual);
        assertNotEquals(fromSecondToFirst, actual);
    }

    @Test
    public void readById() {
        long id = entityManager.persistAndGetId(fromFirstToSecond, Long.class);
        entityManager.flush();

        Message actual = messageDao.getOne(id);

        assertEquals(fromFirstToSecond, actual);
        assertNotEquals(fromSecondToFirst, actual);
    }




    @Test
    public void deleteById() {
        long deletedId = (Long) entityManager.persistAndGetId(fromFirstToSecond);
        entityManager.persist(fromSecondToFirst);
        entityManager.persist(fromFirstToThird);
        entityManager.persist(fromSecondToThird);
        entityManager.flush();

        messageDao.delete(deletedId);
        Message actual = messageDao.findOne(deletedId);

        assertNull(actual);

        long actualCount = messageDao.count();
        long expectedCount = 3;

        assertEquals(expectedCount, actualCount);
    }

    @SuppressWarnings("Duplicates")
    @Test
    public void deleteByObject() {
        entityManager.persist(fromFirstToSecond);
        entityManager.persist(fromSecondToFirst);
        entityManager.persist(fromFirstToThird);
        entityManager.persist(fromSecondToThird);
        entityManager.flush();

        messageDao.delete(fromFirstToSecond);

        long actualCount = messageDao.count();
        long expectedCount = 3;

        assertEquals(expectedCount, actualCount);
    }


    @Test
    public void findAll() {
        entityManager.persist(fromFirstToSecond);
        entityManager.persist(fromSecondToFirst);
        entityManager.persist(fromFirstToThird);
        entityManager.persist(fromSecondToThird);
        entityManager.flush();

        List<Message> founded = messageDao.findAll();
        long expectedCount = 4;

        assertEquals(expectedCount, founded.size());

    }

    @Test
    public void findAllBySenderAndRecipient() {
        entityManager.persist(fromFirstToSecond);
        entityManager.persist(fromSecondToFirst);
        entityManager.persist(fromFirstToThird);
        entityManager.persist(fromSecondToThird);
        entityManager.flush();

        List<Message> founded = messageDao.getAllBySenderAndRecipient(firstAccount, secondAccount);
        long expectedCount = 1;

        assertEquals(expectedCount, founded.size());

    }
}
