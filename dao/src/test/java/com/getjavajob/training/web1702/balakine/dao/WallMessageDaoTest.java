package com.getjavajob.training.web1702.balakine.dao;


import com.getjavajob.training.web1702.balakine.dao.config.DaoConfig;
import com.getjavajob.training.web1702.balakine.model.Account;
import com.getjavajob.training.web1702.balakine.model.WallMessage;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNull;

@RunWith(SpringRunner.class)
@DataJpaTest
@ContextConfiguration(classes = DaoConfig.class)
public class WallMessageDaoTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private WallMessageDao wallMessageDao;

    private Account firstAccount;
    private Account secondAccount;
    private Account thirdAccount;

    private WallMessage fromFirstToSecond;
    private WallMessage fromSecondToFirst;
    private WallMessage fromFirstToThird;
    private WallMessage fromFirstToFirst;

    @Before
    public void init(){
        firstAccount = Account.AccountBuilder.anAccount()
                .withMail("firstAccount@mail.ru")
                .withPassword("1111")
                .withFirstName("firstAccountFirstName")
                .withSecondName("firstAccountSecondName")
                .withThirdName("firstAccountThirdName")
                .withSkype("skype")
                .withIcq("11111111")
                .withHomeAddress("homeAddress")
                .withWorkAddress("workAddress")
                .withBirthday(LocalDate.parse("1991-04-01"))
                .withDateOfRegistration(LocalDate.parse("2003-01-01"))
                .build();

        secondAccount = Account.AccountBuilder.anAccount()
                .withMail("secondAccount@mail.ru")
                .withPassword("222")
                .withFirstName("secondAccountFirstName")
                .withSecondName("secondAccountSecondName")
                .withThirdName("secondAccountThirdName")
                .withSkype("skype2")
                .withIcq("2222222")
                .withHomeAddress("homeAddress2")
                .withWorkAddress("workAddress2")
                .withBirthday(LocalDate.parse("1991-04-02"))
                .withDateOfRegistration(LocalDate.parse("2003-01-02"))
                .build();

        thirdAccount = Account.AccountBuilder.anAccount()
                .withMail("thirdAccount@mail.ru")
                .withPassword("333")
                .withFirstName("thirdAccountFirstName")
                .withSecondName("thirdAccountSecondName")
                .withThirdName("thirdAccountThirdName")
                .withSkype("skype3")
                .withIcq("33333333333")
                .withHomeAddress("homeAddress3")
                .withWorkAddress("workAddress3")
                .withBirthday(LocalDate.parse("1991-04-03"))
                .withDateOfRegistration(LocalDate.parse("2003-01-03"))
                .build();

        entityManager.persist(firstAccount);
        entityManager.persist(secondAccount);
        entityManager.persist(thirdAccount);
        entityManager.flush();

        fromFirstToSecond = WallMessage.WallMessageBuilder.aWallMessage()
                .withMessage("fromFirstToSecond")
                .withOwner(firstAccount)
                .withWriter(secondAccount)
                .build();

        fromSecondToFirst = WallMessage.WallMessageBuilder.aWallMessage()
                .withMessage("fromSecondToFirst")
                .withOwner(secondAccount)
                .withWriter(firstAccount)
                .build();

        fromFirstToThird = WallMessage.WallMessageBuilder.aWallMessage()
                .withMessage("fromFirstToThird")
                .withOwner(firstAccount)
                .withWriter(thirdAccount)
                .build();

        fromFirstToFirst = WallMessage.WallMessageBuilder.aWallMessage()
                .withMessage("fromSecondToThird")
                .withOwner(firstAccount)
                .withWriter(firstAccount)
                .build();

    }

    @Test
    public void createOk() {
        WallMessage savedMessage = wallMessageDao.saveAndFlush(fromFirstToSecond);

        WallMessage actual = entityManager.find(WallMessage.class, savedMessage.getId());

        assertEquals(fromFirstToSecond, actual);
        assertNotEquals(fromSecondToFirst, actual);
    }

    @Test
    public void readById() {
        long id = entityManager.persistAndGetId(fromFirstToSecond, Long.class);
        entityManager.flush();

        WallMessage actual = wallMessageDao.getOne(id);

        assertEquals(fromFirstToSecond, actual);
        assertNotEquals(fromSecondToFirst, actual);
    }


    @Test
    public void update() {
        WallMessage message = entityManager.persistAndFlush(fromFirstToSecond);

        WallMessage oldMessage = wallMessageDao.getOne(message.getId());

        assertEquals(fromFirstToSecond, oldMessage);

        message.setMessage("updateMessageFromFirstToSecond");
        wallMessageDao.save(message);

        WallMessage newMessage = wallMessageDao.getOne(message.getId());

        assertEquals(message, newMessage);

    }


    @Test
    public void deleteById() {
        long deletedId = (Long) entityManager.persistAndGetId(fromFirstToSecond);
        entityManager.persist(fromSecondToFirst);
        entityManager.persist(fromFirstToThird);
        entityManager.persist(fromFirstToFirst);
        entityManager.flush();

        wallMessageDao.delete(deletedId);
        WallMessage actual = wallMessageDao.findOne(deletedId);

        assertNull(actual);

        long actualCount = wallMessageDao.count();
        long expectedCount = 3;

        assertEquals(expectedCount, actualCount);
    }

    @SuppressWarnings("Duplicates")
    @Test
    public void deleteByObject() {
        entityManager.persist(fromFirstToSecond);
        entityManager.persist(fromSecondToFirst);
        entityManager.persist(fromFirstToThird);
        entityManager.persist(fromFirstToFirst);
        entityManager.flush();

        wallMessageDao.delete(fromFirstToSecond);

        long actualCount = wallMessageDao.count();
        long expectedCount = 3;

        assertEquals(expectedCount, actualCount);
    }


    @Test
    public void findAll() {
        entityManager.persist(fromFirstToSecond);
        entityManager.persist(fromSecondToFirst);
        entityManager.persist(fromFirstToThird);
        entityManager.persist(fromFirstToFirst);
        entityManager.flush();

        List<WallMessage> founded = wallMessageDao.findAll();
        long expectedCount = 4;

        assertEquals(expectedCount, founded.size());

    }

    @Test
    public void findAllByOwner() {
        entityManager.persist(fromFirstToSecond);
        entityManager.persist(fromSecondToFirst);
        entityManager.persist(fromFirstToThird);
        entityManager.persist(fromFirstToFirst);
        entityManager.flush();

        List<WallMessage> founded = wallMessageDao.findAllByOwner(firstAccount);
        long expectedCount = 3;

        assertEquals(expectedCount, founded.size());

    }
}
