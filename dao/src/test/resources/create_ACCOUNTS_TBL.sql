DROP TABLE IF EXISTS ACCOUNTS_TBL;
CREATE TABLE ACCOUNTS_TBL
(
  id                 BIGINT      NOT NULL AUTO_INCREMENT,
  mail              VARCHAR(50),
  password           VARCHAR(20) NOT NULL,
  firstName          VARCHAR(20) NOT NULL,
  secondName         VARCHAR(20) NOT NULL,
  thirdName          VARCHAR(20),
  birthday           DATE,
  homeAddress        VARCHAR(20),
  workAddress        VARCHAR(20),
  icq                VARCHAR(20),
  skype              VARCHAR(20),
  dateOfRegistration DATE,
  photo              BLOB,
  PRIMARY KEY (id),
  UNIQUE (mail)
);

INSERT INTO ACCOUNTS_TBL ( id, mail, password, firstName, secondName, thirdName, birthday, homeAddress, workAddress, icq, skype, dateOfRegistration)
VALUES ('1', 'jfdhbvdbv@mail.ru', '123', 'dvkdfnv', 'fdvkdjf', 'fdkdjfb', '1990-04-04', 'homeAddress', 'workAddress',
             '11241234', 'djfbdfb', '2001-01-01');

INSERT INTO ACCOUNTS_TBL ( id, mail, password, firstName, secondName, thirdName, birthday, homeAddress, workAddress, icq, skype, dateOfRegistration)
VALUES ('2', 'kfjbfkjdf@mail.ru', '12345', 'kjdfn', 'kdfjnv', 'dkfjnb', '1991-04-04', 'homeAddress45', 'workAddress45',
             '1124123454', 'djfbdfb45', '2002-01-01');

INSERT INTO ACCOUNTS_TBL ( id, mail, password, firstName, secondName, thirdName, birthday, homeAddress, workAddress, icq, skype, dateOfRegistration)
VALUES ('3' , 'jfebvefr@mail.ru', '12345', 'kjdfn', 'kdfjnv', 'dkfjnb', '1991-04-04', 'homeAddress45', 'workAddress45',
              '1124123454', 'djfbdfb45', '2003-01-01');