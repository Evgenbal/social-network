package com.getjavajob.training.web1702.balakine.service;

import com.getjavajob.training.web1702.balakine.dto.AccountDTO;
import com.getjavajob.training.web1702.balakine.model.Account;
import com.getjavajob.training.web1702.balakine.model.Phone;

import java.util.List;

public interface AccountService extends Service<Account, Long> {

    /**
     * Getting Account from database by mail
     *
     * @param mail - Account to find
     * @return founded Account
     */
    Account readByMail(String mail);
    /**
     * Get accounts from database by filter in a given range
     *
     * @param count      - count of elements to find in range
     * @param last       - index of last element in range
     * @param searchWord - word to search
     * @return list of founded entities optimise for search
     */
    List<AccountDTO> searchByWord(int page, int size, String searchWord);

    /**
     * Get accounts from database by filter with default values
     *
     * @param searchWord - word to search
     * @return list of founded entities optimise for search
     */
    List<AccountDTO> searchByWord(String searchWord);

    /**
     * Get total count of founded accounts by filter
     *
     * @param searchWord - word to search
     * @return count of founded entities
     */
    long getTotalNumberFoundedEntities(String searchWord);

    /**
     * Updates Account with List of phones.
     * If some elements in the List are null or filed is null
     * this element will be removed from the list
     * Use this method to prevent exceptions.
     * @param obj - obj to update
     * @param phones - phones to set in account
     */
    void updateWithNewPhone(Account obj, List<Phone> phones);
}
