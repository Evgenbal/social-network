package com.getjavajob.training.web1702.balakine.service;

import com.getjavajob.training.web1702.balakine.dto.AccountDTO;
import com.getjavajob.training.web1702.balakine.model.Account;
import com.getjavajob.training.web1702.balakine.model.Friends;
import com.getjavajob.training.web1702.balakine.service.Utils.FriendStatus;

import java.util.List;

public interface FriendService extends Service<Friends, Long> {

    /**
     *  Return all friends of Account
     * Provided that both from him and to him there is a request to the person
     *
     * @param accountId - Account for getting it`s requests
     * @return list of Accounts
     */
    List<AccountDTO> getFriends(long accountId);

    /**
     * Gets all incoming requests to friends
     * if there are no outbound requests to friends from the same accounts
     * Accounts sorted by date of send requests
     * @param obj - Account for getting requests to him
     * @return list of Accounts for incoming requests
     */
    List<AccountDTO> getRequests(Account obj);

    /**
     * Gets all outgoing requests to friends
     * if there are no incoming requests to friends from the same accounts
     * Accounts sorted by date of send requests
     * @param obj
     * @return list of Accounts for outgoing requests
     */

    List<AccountDTO> getOutgoingFriends(Account obj);

    /**
     * Creating new outgoing request to friend from this account
     * @param accountInSession - this account
     * @param accountFromPageId - friend account id
     */
    void addNewFriend(Account accountInSession, long accountFromPageId);

    /**
     * Remove outgoing request to friend from this account
     * @param accountInSession - this account
     * @param accountFromPageId - friend account id
     */
    void removeFriend(Account accountInSession, long accountFromPageId);

    /**
     * Getting status between tho accounts.
     * @see FriendStatus
     * @param accountInSession - account in session
     * @param accountFromPage - another account
     * @return - friend status
     */
    FriendStatus getStatusWithPage(Account accountInSession, long accountFromPage);
}
