package com.getjavajob.training.web1702.balakine.service.Impl;

import com.getjavajob.training.web1702.balakine.model.AbstractModel;
import com.getjavajob.training.web1702.balakine.service.Service;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.List;

@Transactional(readOnly = true)

public abstract class AbstractService<T extends AbstractModel, ID extends Serializable>
        implements Service<T, ID> {
    private JpaRepository<T, ID> repository;

    public AbstractService(JpaRepository<T, ID> repository) {
        this.repository = repository;
    }

    @Override
    @Transactional
    public T create(T obj) {
        return repository.save(obj);
    }

    @Override
    public T read(ID id) {
        return repository.findOne(id);
    }

    @Override
    @Transactional
    public void update(T entity) {
        repository.save(entity);

    }

    @Override
    @Transactional
    public void delete(ID id) {
        repository.delete(id);

    }

    @Override
    public List<T> getAll() {
        return repository.findAll();
    }
}
