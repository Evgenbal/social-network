package com.getjavajob.training.web1702.balakine.service.Impl;

import com.getjavajob.training.web1702.balakine.dao.AccountDao;
import com.getjavajob.training.web1702.balakine.dto.AccountDTO;
import com.getjavajob.training.web1702.balakine.model.*;
import com.getjavajob.training.web1702.balakine.service.AccountService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.*;

import static com.getjavajob.training.web1702.balakine.service.Utils.AccountMapper.convertAccountForSearchResults;

@Service
@Transactional(readOnly = true)
public class AccountServiceImpl extends AbstractService<Account, Long> implements AccountService {

    private static final Logger logger = LoggerFactory.getLogger(AccountServiceImpl.class);

    private final AccountDao accountDao;


    @Autowired
    public AccountServiceImpl(AccountDao accountDao) {
        super(accountDao);
        this.accountDao = accountDao;
    }


    @Override
    @Transactional
    public Account readByMail(String mail) {
        return accountDao.findAccountByMail(mail);
    }

    @Override
    public List<AccountDTO> searchByWord(int page, int size, String searchWord) {
        PageRequest pageable = new PageRequest(page, size, new Sort("firstName", "secondName"));
        return convertAccountForSearchResults(accountDao.findByFilterWithPagination(searchWord, pageable));
    }
    @Override
    public List<AccountDTO> searchByWord(String searchWord) {
        int defaultPage = 1;
        int defaultEntitiesPerPage = 4;
        PageRequest pageable = new PageRequest(defaultPage, defaultEntitiesPerPage, new Sort("firstName", "secondName"));
        return convertAccountForSearchResults(accountDao.findByFilterWithPagination(searchWord, pageable));
    }

    @Override
    public long getTotalNumberFoundedEntities(String searchWord) {
        return accountDao.getCountOfAccountsByFilter(searchWord);
    }

    @Override
    @Transactional
    public void updateWithNewPhone(Account obj, List<Phone> phones) {
        phones.removeIf(phone -> phone.getPhoneNumber() == null
                || phone.getPhoneNumber().isEmpty()
                || phone.getUser() == null);
        obj.setPhones(phones);
        accountDao.save(obj);
    }

}
