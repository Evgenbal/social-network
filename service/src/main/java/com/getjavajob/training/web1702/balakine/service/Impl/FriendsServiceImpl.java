package com.getjavajob.training.web1702.balakine.service.Impl;

import com.getjavajob.training.web1702.balakine.dao.AccountDao;
import com.getjavajob.training.web1702.balakine.dao.FriendsDao;
import com.getjavajob.training.web1702.balakine.dto.AccountDTO;
import com.getjavajob.training.web1702.balakine.model.Account;
import com.getjavajob.training.web1702.balakine.model.Friends;
import com.getjavajob.training.web1702.balakine.model.Friends.FriendsBuilder;
import com.getjavajob.training.web1702.balakine.service.FriendService;
import com.getjavajob.training.web1702.balakine.service.Utils.FriendStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.*;

import static com.getjavajob.training.web1702.balakine.service.Utils.AccountMapper.convertAccountForSearchResults;

@Service
@Transactional(readOnly = true)
public class FriendsServiceImpl extends AbstractService<Friends, Long> implements FriendService {

    private final FriendsDao friendsDao;

    private final AccountDao accountDao;

    @Autowired
    public FriendsServiceImpl(FriendsDao friendsDao, AccountDao accountDao) {
        super(friendsDao);
        this.friendsDao = friendsDao;
        this.accountDao = accountDao;
    }


    @Override
    public List<AccountDTO> getFriends(long accountId) {
        List<Account> accountsAcceptors = getAllAcceptorsAndSort(accountId);

        List<Account> accountsRequesters = getAllRequestersAndSort(accountId);

        accountsAcceptors.retainAll(accountsRequesters);

        return convertAccountForSearchResults(accountsAcceptors);
    }


    @Override
    //requests to account
    public List<AccountDTO> getRequests(Account obj) {

        List<Account> requesters = getAllRequestersAndSort(obj.getId());
        List<Account> acceptors = getAllAcceptorsAndSort(obj.getId());

        return convertAccountForSearchResults(getAccountsFromFirstList(requesters, acceptors));
    }

    @Override
    //requests from account
    public List<AccountDTO> getOutgoingFriends(Account obj) {
        List<Account> requesters = getAllRequestersAndSort(obj.getId());
        List<Account> acceptors = getAllAcceptorsAndSort(obj.getId());

        return convertAccountForSearchResults(getAccountsFromFirstList(acceptors, requesters));
    }

    @Override
    @Transactional
    public void addNewFriend(Account accountInSession, long accountFromPageId) {
        Friends friends = FriendsBuilder.aFriends()
                .withFriendRequester(accountInSession)
                .withFriendAccepter(accountDao.findOne(accountFromPageId))
                .withDate(LocalDateTime.now())
                .build();
        friendsDao.save(friends);

    }

    @Override
    @Transactional
    public void removeFriend(Account accountInSession, long accountFromPageId) {
        Friends friends = friendsDao.findFriendShip(accountInSession.getId(), accountFromPageId);
        friendsDao.delete(friends);
    }

    @Override
    public FriendStatus getStatusWithPage(Account accountInSession, long accountFromPageId) {
        long accountId = accountInSession.getId();
        if (friendsDao.findFriendShip(accountId, accountFromPageId) != null
                && friendsDao.findFriendShip(accountFromPageId, accountId) != null) {
            return FriendStatus.FRIEND;
        } else if (friendsDao.findFriendShip(accountId, accountFromPageId) != null) {
            return FriendStatus.WAIT_FOR_ACCEPT;
        } else {
            return FriendStatus.NOT_FRIEND;
        }
    }

    private List<Account> getAccountsFromFirstList(List<Account> first, List<Account> second) {
        first.removeAll(second);
        return first;
    }

    //all requests to account
    private List<Account> getAllRequestersAndSort(long accountID) {
        List<Friends> requestsToAccount = friendsDao.findAllByFriendAccepter_AccountId(accountID);
        sortFriends(requestsToAccount);
        return retrieveAccountFromFriend(requestsToAccount, accountID);
    }

    //all requests from account
    private List<Account> getAllAcceptorsAndSort(long accountID) {
        List<Friends> requestsFromAccount = friendsDao.findAllByFriendRequester_AccountId(accountID);
        sortFriends(requestsFromAccount);
        return retrieveAccountFromFriend(requestsFromAccount, accountID);
    }

    /**
     * Sorting friends in list by date
     *
     * @param relationsToSort - list of friends to sort
     */

    private void sortFriends(List<Friends> relationsToSort) {
        relationsToSort.sort(Comparator.comparing(Friends::getDate));
    }

    /**
     * Extracts from the list other accounts in which the requester or acceptor is the same for the entire list
     * Other accounts can be both requester and acceptors of friendship
     *
     * @param relations     - list with friends
     * @param mainAccountID - account id which is the same for the whole list
     * @return List of accounts
     */

    private List<Account> retrieveAccountFromFriend(Collection<Friends> relations, long mainAccountID) {
        List<Account> accounts = new ArrayList<>();
        for (Friends friend : relations) {
            if (friend.getFriendAccepter().getId() == mainAccountID) {
                accounts.add(friend.getFriendRequester());
            } else {
                accounts.add(friend.getFriendAccepter());
            }
        }
        return accounts;
    }

}
