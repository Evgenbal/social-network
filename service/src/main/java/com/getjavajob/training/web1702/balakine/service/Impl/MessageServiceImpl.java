package com.getjavajob.training.web1702.balakine.service.Impl;

import com.getjavajob.training.web1702.balakine.dao.AccountDao;
import com.getjavajob.training.web1702.balakine.dao.ChatChannelDao;
import com.getjavajob.training.web1702.balakine.dao.MessageDao;
import com.getjavajob.training.web1702.balakine.dto.InitChatChannelDTO;
import com.getjavajob.training.web1702.balakine.dto.ChatMessageDTO;
import com.getjavajob.training.web1702.balakine.model.Account;
import com.getjavajob.training.web1702.balakine.model.ChatChannel;
import com.getjavajob.training.web1702.balakine.model.Message;
import com.getjavajob.training.web1702.balakine.model.Message.MessageBuilder;
import com.getjavajob.training.web1702.balakine.service.MessageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

import static com.getjavajob.training.web1702.balakine.service.Utils.MessageMapper.convertMessageListToDtoList;
import static com.getjavajob.training.web1702.balakine.service.Utils.MessageMapper.convertMessageToDto;

@Service
@Transactional(readOnly = true)
public class MessageServiceImpl extends AbstractService<Message, Long> implements MessageService {

    private static final Logger logger = LoggerFactory.getLogger(MessageServiceImpl.class);

    private final MessageDao messageDao;
    private final AccountDao accountDao;

    private final ChatChannelDao channelDao;

    @Autowired
    public MessageServiceImpl(MessageDao messageDao, AccountDao accountDao, ChatChannelDao channelDao) {
        super(messageDao);
        this.messageDao = messageDao;
        this.accountDao = accountDao;
        this.channelDao = channelDao;
    }

    @Override
    @Transactional
    public ChatMessageDTO addMessage(ChatMessageDTO chatMessageDTO) {

        Message message = MessageBuilder.aMessage()
                .withMessage(chatMessageDTO.getText())
                 .withDateOfMessage(LocalDateTime.now())
                .withSender(Account.AccountBuilder.anAccount().withAccountId(chatMessageDTO.getSenderId()).build())
                .withRecipient(Account.AccountBuilder.anAccount().withAccountId(chatMessageDTO.getRecipientId()).build())
                .build();

        logger.debug(message.toString());

        return convertMessageToDto(messageDao.save(message));

    }

    @Override
    public List<ChatMessageDTO> getAllMessagesWithFriend(Account accountInSession, long friendId) {
        List<Message> messages = messageDao.getAllBySenderAndRecipient(accountInSession, accountDao.findOne(friendId));
        messages.addAll(messageDao.getAllBySenderAndRecipient(accountDao.findOne(friendId), accountInSession));
        return convertMessageListToDtoList(messages);
    }

    @Override
    @Transactional
    public String initChatSession(InitChatChannelDTO initChatChannelDTO) {
        String uuid = getExistingChannel(initChatChannelDTO);
        logger.debug("UUID " + uuid);
        return (uuid == null ? createChatChannel(initChatChannelDTO) : uuid);
    }


    private String createChatChannel(InitChatChannelDTO initChatChannelDTO) {
        ChatChannel channel = ChatChannel.ChatChannelBuilder.aChatChannel()
                .withUserOne(accountDao.findOne(initChatChannelDTO.getUserOneId()))
                .withUserTwo(accountDao.findOne(initChatChannelDTO.getUserTwoId()))
                .build();
        ChatChannel savedChannel = channelDao.save(channel);
        logger.debug(savedChannel.toString());
        return channel.getUuid();

    }

    private String getExistingChannel(InitChatChannelDTO initChatChannelDTO) {
        List<ChatChannel> chatChannels = channelDao.getChatChannel(
                initChatChannelDTO.getUserOneId(),
                initChatChannelDTO.getUserTwoId()
        );

        logger.debug("Chat channel exists " + (chatChannels != null && !chatChannels.isEmpty()));
        return (chatChannels != null && !chatChannels.isEmpty() ? chatChannels.get(0).getUuid() : null);

    }
}
