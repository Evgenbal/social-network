package com.getjavajob.training.web1702.balakine.service.Impl;

import com.getjavajob.training.web1702.balakine.dao.AccountDao;
import com.getjavajob.training.web1702.balakine.dao.WallMessageDao;
import com.getjavajob.training.web1702.balakine.dto.WallMessageDTO;
import com.getjavajob.training.web1702.balakine.model.Account;
import com.getjavajob.training.web1702.balakine.model.WallMessage;
import com.getjavajob.training.web1702.balakine.service.Utils.WallMessageMapper;
import com.getjavajob.training.web1702.balakine.service.WallMessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

import static com.getjavajob.training.web1702.balakine.service.Utils.WallMessageMapper.*;

@Service
@Transactional(readOnly = true)
public class WallMessageServiceImpl extends AbstractService<WallMessage, Long> implements WallMessageService {
    private final WallMessageDao wallMessageDao;

    private final AccountDao accountDao;

    @Autowired
    public WallMessageServiceImpl(WallMessageDao wallMessageDao, AccountDao accountDao) {
        super(wallMessageDao);
        this.wallMessageDao = wallMessageDao;
        this.accountDao = accountDao;
    }

    @Override
    public List<WallMessageDTO> getAllMessages(Account account) {
        return convertWallMessageListToDto(wallMessageDao.findAllByOwner(account));
    }

    @Override
    public List<WallMessageDTO> getAllMessages(long accountId) {
        Account account = accountDao.findOne(accountId);
        return convertWallMessageListToDto(wallMessageDao.findAllByOwner(account));
    }

    @Override
    @Transactional
    public WallMessageDTO saveWallMessage(Account writer, Long ownerId, String text) {
        WallMessage wallMessage = WallMessage.WallMessageBuilder.aWallMessage()
                .withOwner(accountDao.findOne(ownerId))
                .withDateOfMessage(LocalDateTime.now())
                .withWriter(writer)
                .withMessage(text)
                .build();
        WallMessage wallMessageSaved = wallMessageDao.save(wallMessage);
        return WallMessageMapper.convertWallMessageToDto(wallMessageSaved);
    }


}
