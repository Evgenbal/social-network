package com.getjavajob.training.web1702.balakine.service;

import com.getjavajob.training.web1702.balakine.dto.InitChatChannelDTO;
import com.getjavajob.training.web1702.balakine.dto.ChatMessageDTO;
import com.getjavajob.training.web1702.balakine.model.Account;

import java.util.List;

public interface MessageService {

    /**
     * Get all messages between to accounts
     * Message are sorted by date of send
     * @param accountInSession -
     * @param friend
     * @return
     */
    List<ChatMessageDTO> getAllMessagesWithFriend(Account accountInSession, long friendId);

    /**
     * Add new message
     * @param chatMessageDTO - message to save
     *
     */
    ChatMessageDTO addMessage(ChatMessageDTO chatMessageDTO);

    /**
     * Creates new private channel between or return existing channel
     * @param initChatChannelDTO - channel to find or return
     * @return uuid of found/created channel
     */

    String initChatSession(InitChatChannelDTO initChatChannelDTO);
}
