package com.getjavajob.training.web1702.balakine.service;

import com.getjavajob.training.web1702.balakine.model.Account;
import com.getjavajob.training.web1702.balakine.model.CustomUserDetails;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;

@Component
public class UserDetailsServiceImpl implements UserDetailsService {
    private static final Logger logger = LoggerFactory.getLogger(UserDetailsServiceImpl.class);

    @Autowired
    private Service<Account> accountService;

    @Override
    public UserDetails loadUserByUsername(String mail) throws UsernameNotFoundException {
        Account account = new Account();
        account.setMail(mail);
        Account foundedAccount = accountService.readByObj(account);
        if (foundedAccount == null) {
            throw new UsernameNotFoundException("Account not found");
        }
        return new CustomUserDetails(foundedAccount);

        /*return new User(foundedAccount.getMail(), foundedAccount.getPassword()
                , true, true, true, true,
                Collections.singleton(new SimpleGrantedAuthority("ROLE_" + foundedAccount.getRole())));*/
    }
}
