package com.getjavajob.training.web1702.balakine.service.Utils;

import com.getjavajob.training.web1702.balakine.dto.AccountDTO;
import com.getjavajob.training.web1702.balakine.model.Account;

import java.util.List;
import java.util.stream.Collectors;

import static com.getjavajob.training.web1702.balakine.service.Utils.AccountPhotoConverter.getBase64Photo;
import static com.getjavajob.training.web1702.balakine.service.Utils.AccountPhotoConverter.getPhotoByteArray;

public class AccountMapper {
    public static List<AccountDTO> convertAccountForSearchResults(List<Account> accounts) {
        return accounts.stream()
                .map(a -> AccountDTO.AccountDTOBuilder.anAccountDTO()
                        .withAccountId(a.getId())
                        .withFirstName(a.getFirstName())
                        .withSecondName(a.getSecondName())
                        .withPhoto(getBase64Photo(getPhotoByteArray(a)))
                        .build())
                .collect(Collectors.toList());
    }

    public static AccountDTO convertAccountToDTO(Account account) {
        return AccountDTO.AccountDTOBuilder.anAccountDTO()
                .withAccountId(account.getId())
                .withFirstName(account.getFirstName())
                .withSecondName(account.getSecondName())
                .withPhoto(getBase64Photo(getPhotoByteArray(account)))
                .build();
    }
}
