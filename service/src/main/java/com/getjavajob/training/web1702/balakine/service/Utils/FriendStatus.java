package com.getjavajob.training.web1702.balakine.service.Utils;


public enum FriendStatus {
    FRIEND("friend"),
    NOT_FRIEND("notFriend"),
    WAIT_FOR_ACCEPT("waitForAccept");

    private final String val;

    FriendStatus(String val) {
        this.val = val;
    }

    public String getStatus() {
        return val;
    }
}
