package com.getjavajob.training.web1702.balakine.service.Utils;

import com.getjavajob.training.web1702.balakine.dto.ChatMessageDTO;
import com.getjavajob.training.web1702.balakine.model.Message;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static com.getjavajob.training.web1702.balakine.service.Utils.AccountPhotoConverter.getPhotoByteArray;

public class MessageMapper {

    public static List<ChatMessageDTO> convertMessageListToDtoList(List<Message> messages) {
        return messages.stream()
                .map(m -> ChatMessageDTO.ChatMessageDTOBuilder.aChatMessageDTO()
                        .withSenderId(m.getSender().getId())
                        .withRecipientId(m.getRecipient().getId())
                        .withText(m.getMessageText())
                        .withTimeOfSend(convertLocalDateTime(m.getDateOfMessage()))
                        .build())
                .sorted(Comparator.comparing(ChatMessageDTO::getTimeOfSend))
                .collect(Collectors.toList());
    }


    public static ChatMessageDTO convertMessageToDto(Message message) {
        return ChatMessageDTO.ChatMessageDTOBuilder.aChatMessageDTO()
                .withSenderId(message.getSender().getId())
                .withRecipientId(message.getRecipient().getId())
                .withText(message.getMessageText())
                .withTimeOfSend(convertLocalDateTime(message.getDateOfMessage()))
                .build();
    }


    private static Date convertLocalDateTime(LocalDateTime localDateTime) {
        Instant instant = localDateTime.atZone(ZoneId.systemDefault()).toInstant();
        return Date.from(instant);
    }


}
