package com.getjavajob.training.web1702.balakine.service.Utils;

import com.getjavajob.training.web1702.balakine.model.Account;
import com.getjavajob.training.web1702.balakine.service.AccountService;
import com.getjavajob.training.web1702.balakine.service.Service;
import com.getjavajob.training.web1702.balakine.utils.CustomUserDetails;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

@Component
public class UserDetailsServiceImpl implements UserDetailsService {

    private static final Logger logger = LoggerFactory.getLogger(UserDetailsServiceImpl.class);

    private final AccountService accountService;

    @Autowired
    public UserDetailsServiceImpl(AccountService accountService) {
        this.accountService = accountService;
    }

    @Override
    public UserDetails loadUserByUsername(String mail) throws UsernameNotFoundException {

        logger.debug("Try to find account with mail: " + mail);
        Account foundedAccount = accountService.readByMail(mail);
        if (foundedAccount == null) {
            throw new UsernameNotFoundException("Account not found");
        }
        logger.debug("Account founded with id " + foundedAccount.getId());
        return new CustomUserDetails(foundedAccount);
    }
}
