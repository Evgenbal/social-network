package com.getjavajob.training.web1702.balakine.service.Utils;

import com.getjavajob.training.web1702.balakine.dto.WallMessageDTO;
import com.getjavajob.training.web1702.balakine.model.WallMessage;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static com.getjavajob.training.web1702.balakine.service.Utils.AccountPhotoConverter.getBase64Photo;
import static com.getjavajob.training.web1702.balakine.service.Utils.AccountPhotoConverter.getPhotoByteArray;

public class WallMessageMapper {

    public static List<WallMessageDTO> convertWallMessageListToDto(List<WallMessage> messages) {
        return messages.stream()
                .map(m -> WallMessageDTO.WallMessageDTOBuilder.aWallMessageDTO()
                        .withMessageId(m.getId())
                        .withSenderId(m.getWriter().getId())
                        .withSenderFirstName(m.getWriter().getFirstName())
                        .withSenderSecondName(m.getWriter().getSecondName())
                        .withSenderPhoto(getBase64Photo(getPhotoByteArray(m.getWriter())))
                        .withText(m.getMessage())
                        .withPostTime(m.getDateOfMessage())
                        .build())
                .sorted(Comparator.comparing(WallMessageDTO::getPostTime).reversed())
                .collect(Collectors.toList());
    }

    public static WallMessageDTO convertWallMessageToDto(WallMessage wallMessage) {
        return WallMessageDTO.WallMessageDTOBuilder.aWallMessageDTO()
                .withMessageId(wallMessage.getId())
                .withSenderId(wallMessage.getWriter().getId())
                .withSenderFirstName(wallMessage.getWriter().getFirstName())
                .withSenderSecondName(wallMessage.getWriter().getSecondName())
                .withSenderPhoto(getBase64Photo(getPhotoByteArray(wallMessage.getWriter())))
                .withText(wallMessage.getMessage())
                .withPostTime(wallMessage.getDateOfMessage())
                .build();
    }

}
