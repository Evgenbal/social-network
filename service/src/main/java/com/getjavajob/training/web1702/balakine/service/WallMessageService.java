package com.getjavajob.training.web1702.balakine.service;

import com.getjavajob.training.web1702.balakine.dto.WallMessageDTO;
import com.getjavajob.training.web1702.balakine.model.Account;
import com.getjavajob.training.web1702.balakine.model.WallMessage;

import java.util.List;

public interface WallMessageService extends Service<WallMessage, Long> {

    /**
     * Returns all account`s wall posts
     * @return all posts on account`s wall
     */
    List<WallMessageDTO> getAllMessages(Account account);


    /**
     * Returns all account`s wall posts
     * @return all posts on account`s wall
     */
    List<WallMessageDTO> getAllMessages(long accountId);


    /**
     * Save wall post to database
     * @param ownerId - owner of the wall
     * @param writer - writer of the post
     * @param text - text of post
     * @return wall post
     */

    WallMessageDTO saveWallMessage(Account writer, Long ownerId, String text);

}
