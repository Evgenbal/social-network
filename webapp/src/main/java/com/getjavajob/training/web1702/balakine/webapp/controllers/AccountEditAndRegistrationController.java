package com.getjavajob.training.web1702.balakine.webapp.controllers;

import com.getjavajob.training.web1702.balakine.model.Account;
import com.getjavajob.training.web1702.balakine.service.AccountService;
import com.getjavajob.training.web1702.balakine.utils.AccountJsonResponse;
import com.getjavajob.training.web1702.balakine.utils.AccountRole;
import com.getjavajob.training.web1702.balakine.utils.CustomUserDetails;
import com.getjavajob.training.web1702.balakine.model.Phone;
import com.getjavajob.training.web1702.balakine.webapp.utils.AccountConverter;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.*;
import java.time.LocalDate;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.getjavajob.training.web1702.balakine.webapp.utils.AccountHelper.*;

@Controller
@SessionAttributes({"accountInSession", "base64Photo"})
public class AccountEditAndRegistrationController {

    private static final Logger logger = LoggerFactory.getLogger(AccountEditAndRegistrationController.class);

    private final AccountService accountService;

    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    public AccountEditAndRegistrationController(AccountService accountService, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.accountService = accountService;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }


    @PostMapping(path = {"/registration"})
    public ModelAndView registration(@ModelAttribute Account account) {

        account.setPassword(bCryptPasswordEncoder.encode(account.getPassword()));
        account.setDateOfRegistration(LocalDate.now());
        account.setRole(AccountRole.USER);

        accountService.create(account);
        logger.info("created new account = " + account);
        ModelAndView modelAndView = new ModelAndView();

        authenticateUser(new CustomUserDetails(account));

        modelAndView.setViewName("/account");
        return setAttributes(modelAndView, account);

    }

    @PostMapping(path = {"/account/editMainInfo"})
    @ResponseBody
    public AccountJsonResponse editMainInfo(
            @ModelAttribute("accountInSession") Account accountInSession,
            @ModelAttribute("accountEdited") @Valid Account accountEdited,
            @RequestParam(value = "newPhoto", required = false) MultipartFile file,
            @RequestParam(value = "deletePhoto", required = false) Object deleteParam,
            BindingResult result) {

        logger.debug("Account to update: " + accountEdited);
        if (result.hasErrors()) {
            Map<String, String> errors = result.getFieldErrors().stream()
                    .collect(
                            Collectors.toMap(FieldError::getField, FieldError::getDefaultMessage)
                    );
            return AccountJsonResponse.AccountJsonResponseBuilder.anAccountJsonResponse()
                    .withValidated(false)
                    .withErrors(errors)
                    .build();

        } else {
            updateAccountFields(accountEdited, accountInSession);
            accountInSession.setPhoto(getPhoto(deleteParam, file, accountInSession));

            accountService.updateWithNewPhone(accountInSession, accountInSession.getPhones());

            return AccountJsonResponse.AccountJsonResponseBuilder.anAccountJsonResponse()
                    .withValidated(true)
                    .withAccount(accountInSession)
                    .build();

        }
    }

    @PostMapping(path = {"/account/editSecurity"})
    @ResponseBody
    public int editSecurity(@RequestParam(value = "newPassword", required = false) String newPassword,
                            @RequestParam("oldPassword") String oldPassword,
                            @RequestParam("mail") String mail,
                            @ModelAttribute("accountInSession") Account accountInSession) {

        Optional<String> newPassOpt = Optional.of(newPassword);
        logger.debug("Change security: newPass: " + newPassOpt.get()
                + " oldPass: " + oldPassword
                + " newMail: " + mail);
        logger.debug(Boolean.toString(!mail.equalsIgnoreCase(accountInSession.getMail()) || !oldPassword.equals(newPassOpt)));
        if (!mail.equalsIgnoreCase(accountInSession.getMail()) || !oldPassword.equals(newPassOpt)) {
            if (!bCryptPasswordEncoder.matches(oldPassword,
                    ((Account) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getPassword())) {
                return 1;
            }
            Optional<Account> account = Optional.of(accountService.readByMail(mail));
            if (account.isPresent() && !account.get().getMail().equals(mail)) {

                return 2;
            } else {
                accountInSession.setMail(mail);
                if (newPassOpt.isPresent()) {
                    accountInSession.setPassword(bCryptPasswordEncoder.encode(newPassword));
                }
                authenticateUser(new CustomUserDetails(accountInSession));
                accountService.update(accountInSession);
                return 3;
            }
        }
        return 0;

    }

    //XML
    @GetMapping(path = "/account/downloadAccount")
    @ResponseBody
    public void toXML(@ModelAttribute("accountInSession") Account account, HttpServletResponse response) {
        logger.debug("Start to write account in xml " + account);
        XStream xStream = new XStream(new DomDriver());
        xStream.alias("account", Account.class);
        xStream.alias("phone", Phone.class);
        xStream.registerConverter(new AccountConverter());
        String accountXml = xStream.toXML(account);
        response.setContentType("application/xml");
        response.addHeader("Content-Disposition", "attachment; filename=account.xml");
        try {
            IOUtils.copy(new BufferedInputStream(new ByteArrayInputStream(accountXml.getBytes())), response.getOutputStream());
            response.getOutputStream().flush();
            logger.debug("Finish to write account in xml " + account);
        } catch (IOException e) {
            logger.warn("cannot write in xml. Account: " + account, e);
        }
    }

    @PostMapping(path = "/account/loadFromXml")
    @ResponseBody
    public int fromXML(@RequestParam("xmlFile") MultipartFile file,
                       @ModelAttribute("accountInSession") Account accountInSession) {
        logger.debug("Start to write account from xml");

        logger.info(file.getOriginalFilename());
        String content = "";
        try {
            content = new String(file.getBytes(), "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }
        logger.info("file " + content);
        logger.info("Size : ", file.getSize());

        XStream xStream = new XStream(new DomDriver());
        xStream.alias("account", Account.class);
        xStream.alias("phone", Phone.class);
        xStream.registerConverter(new AccountConverter());
        Account accountFromXml = (Account) xStream.fromXML(content);
        updateAccountFields(accountFromXml, accountInSession);
        logger.debug("Finish write account from xml. Account: " + accountFromXml);
        return 0;
    }

    private void updateAccountFields(Account newAccount, Account oldAccount) {
        oldAccount.setFirstName(newAccount.getFirstName());

        oldAccount.setSecondName(newAccount.getSecondName());

        oldAccount.setThirdName(newAccount.getThirdName());

        oldAccount.setBirthday(newAccount.getBirthday());

        oldAccount.setHomeAddress(newAccount.getHomeAddress());

        oldAccount.setWorkAddress(newAccount.getWorkAddress());

        oldAccount.setSkype(newAccount.getSkype());

        oldAccount.setIcq(newAccount.getIcq());

        oldAccount.setPhones(newAccount.getPhones());

    }


    private byte[] getPhoto(Object deleteParam, MultipartFile file, Account accountInSession) {
        if (deleteParam != null) {
            return null;
        }
        if (file != null && !file.isEmpty()) {
            try {
                return file.getBytes();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            return accountInSession.getPhoto();
        }
        return null;
    }

    @ModelAttribute("base64Photo")
    public byte[] initBase64Photo() {
        return null;
    }

    private File convertToFile(MultipartFile multipartFile) {
        File file = new File(multipartFile.getOriginalFilename());
        try {
            multipartFile.transferTo(file);
        } catch (IOException e) {
            logger.warn("cannot convert multipartFile to file:" + file.getName(), e);
        }
        return file;
    }

    public void authenticateUser(CustomUserDetails user) {
        SecurityContextHolder.getContext().setAuthentication(
                new UsernamePasswordAuthenticationToken(
                        user.getAccount(),
                        user.getPassword(),
                        user.getAuthorities()));
    }

}

