package com.getjavajob.training.web1702.balakine.webapp.controllers;

import com.getjavajob.training.web1702.balakine.dto.AccountDTO;
import com.getjavajob.training.web1702.balakine.model.Account;
import com.getjavajob.training.web1702.balakine.service.FriendService;
import com.getjavajob.training.web1702.balakine.service.Utils.FriendStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.getjavajob.training.web1702.balakine.webapp.utils.SecurityUtil.getAccountFromSecurityContext;
import static com.getjavajob.training.web1702.balakine.webapp.utils.SecurityUtil.isCurrentAuthenticated;

@RestController
@RequestMapping(value = "/account/restFriends/", produces = "application/json;charset=UTF-8")
public class FriendController {
    private static final Logger logger = LoggerFactory.getLogger(FriendController.class);

    private final FriendService friendService;

    @Autowired
    public FriendController(FriendService friendService) {
        this.friendService = friendService;
    }

    //get friends for logged in account
    @RequestMapping(method = RequestMethod.GET)
    public List<AccountDTO> getFriends() {
        Account currentUser = getAccountFromSecurityContext();
        logger.debug("Start retrieving friends for account with id " + currentUser.getId());

        List<AccountDTO> accounts = friendService.getFriends(currentUser.getId());
        logger.debug("Friends for account with id " + currentUser.getId() + " found. Size: " + accounts.size());
        return accounts;
    }

    //get friends for another account by id
    @RequestMapping(method = RequestMethod.GET, value = "{id}")
    public List<AccountDTO> getFriends(@PathVariable("id") Long id) {
        logger.debug("Start retrieving friends for account with id " + id);

        List<AccountDTO> accounts = friendService.getFriends(id);
        logger.debug("Friends for account with id %d found. Size %d", id, accounts.size());
        return accounts;
    }


    //get status with another account
    @RequestMapping(method = RequestMethod.GET, value = "status/{id}")
    public FriendStatus getStatus(@PathVariable("id") Long id) {
        if (isCurrentAuthenticated()) {
            Account currentUser = getAccountFromSecurityContext();


            logger.debug("get friendships: account id " + currentUser.getId() + "; friend id " + id);

            FriendStatus status = friendService.getStatusWithPage(currentUser, id);

            logger.debug("Status: account id " + currentUser.getId() + "; friend id " + id + " : " + status);

            return status;
        }
        return null;

    }

    @RequestMapping(method = RequestMethod.DELETE, value = "{id}")
    public FriendStatus deleteFriend(@PathVariable("id") Long id) {
        Account currentUser = getAccountFromSecurityContext();

        logger.debug("start remove friendship: account id: " + currentUser.getId() + "; friend id: " + id);
        friendService.removeFriend(currentUser, id);
        logger.debug("removed friendship: account id: " + currentUser.getId() + "; friend id: " + id);

        return friendService.getStatusWithPage(currentUser, id);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "{id}")
    public FriendStatus newFriend(@PathVariable("id") Long id) {
        Account currentUser = getAccountFromSecurityContext();

        logger.debug("start add friendship: account id: " + currentUser.getId() + "; friend id: " + id);
        friendService.addNewFriend(currentUser, id);
        logger.debug("added friendship: account id: " + currentUser.getId() + "; friend id: " + id);

        return friendService.getStatusWithPage(currentUser, id);

    }


}
