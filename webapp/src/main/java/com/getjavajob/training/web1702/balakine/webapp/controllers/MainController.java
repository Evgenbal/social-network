package com.getjavajob.training.web1702.balakine.webapp.controllers;

import com.getjavajob.training.web1702.balakine.model.Account;
import com.getjavajob.training.web1702.balakine.service.AccountService;
import com.getjavajob.training.web1702.balakine.service.FriendService;
import com.getjavajob.training.web1702.balakine.service.WallMessageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationTrustResolver;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import static com.getjavajob.training.web1702.balakine.webapp.utils.AccountHelper.setAttributes;
import static com.getjavajob.training.web1702.balakine.service.Utils.AccountPhotoConverter.getBase64Photo;
import static com.getjavajob.training.web1702.balakine.service.Utils.AccountPhotoConverter.getPhotoByteArray;
import static com.getjavajob.training.web1702.balakine.webapp.utils.SecurityUtil.*;

@Controller
@SessionAttributes({"accountInSession", "base64Photo"})
public class MainController {

    private static final Logger logger = LoggerFactory.getLogger(MainController.class);


    private final AuthenticationTrustResolver authenticationTrustResolver;
    private final AccountService accountService;
    private final FriendService friendService;
    private final WallMessageService wallMessageService;

    @Autowired
    public MainController(AuthenticationTrustResolver authenticationTrustResolver,
                          AccountService accountService,
                          FriendService friendService,
                          WallMessageService wallMessageService) {
        this.authenticationTrustResolver = authenticationTrustResolver;
        this.accountService = accountService;
        this.friendService = friendService;
        this.wallMessageService = wallMessageService;
    }

    @GetMapping(value = {"/login", "/"})
    public String loginPage(@RequestParam(value = "error", required = false) String error,
                                  @RequestParam(value = "logout", required = false) String logout,
                                  @RequestParam(value = "noLogin", required = false) String noLogin,
                                  Model model) {

        if (error != null) {
            model.addAttribute("error", "Incorrect mail and/or password");
        }

        if (logout != null) {
            model.addAttribute("msg", "You've been logged out successfully!");
        }

        if (noLogin != null) {
            model.addAttribute("error", "Please log in to view this page");
        }
        logger.debug("Access to account page" + !isCurrentAuthenticated());
        logger.debug("account in context holder: " + getAccountFromSecurityContext());

        return (isCurrentAuthenticated() ? "/login" : "/account");

    }




    @GetMapping(value = "/logout")
    public String logoutPage() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null) {
            SecurityContextHolder.getContext().setAuthentication(null);
        }
        return "/login";
    }

    @GetMapping(value = "/account")
    public ModelAndView account() {
        logger.debug("Access to account page: " + !isCurrentAuthenticated());
        logger.debug("account in context holder: " + getAccountFromSecurityContext());

        ModelAndView modelAndView = new ModelAndView("/account");

        setAttributes(modelAndView, getAccountFromSecurityContext());
        return modelAndView;
    }

    @GetMapping(value = "/registration")
    public ModelAndView registrationPage() {
        return new ModelAndView("/registration");
    }

    @GetMapping(value = "/account/editAccount")
    public ModelAndView editAccountPage() {
        ModelAndView modelAndView = new ModelAndView("/editAccount");
        setAttributes(modelAndView, getAccountFromSecurityContext());
        return modelAndView;
    }

    @GetMapping(value = "/id{id}")
    public ModelAndView showPage(@PathVariable("id") long id) {

        Account anotherAccount = accountService.read(id);
        ModelAndView modelAndView = new ModelAndView("/viewFoundedPage");

        modelAndView.addObject("foundedAccount", anotherAccount);

        modelAndView.addObject("pagePhoto", getBase64Photo(getPhotoByteArray(anotherAccount)));
        modelAndView.addObject("foundedPage", anotherAccount);

        return modelAndView;
    }


    @RequestMapping(path = "/account/friends")
    public ModelAndView showFriends(@ModelAttribute("accountInSession") Account account) {
        ModelAndView modelAndView = new ModelAndView("/friends");
        modelAndView.addObject("friendsMap", friendService.getFriends(account.getId()));
        modelAndView.addObject("requestsMap", friendService.getRequests(account));
        modelAndView.addObject("outgoingMap", friendService.getOutgoingFriends(account));
        return modelAndView;
    }


}

