package com.getjavajob.training.web1702.balakine.webapp.controllers;

import com.getjavajob.training.web1702.balakine.dto.AccountDTO;
import com.getjavajob.training.web1702.balakine.dto.InitChatChannelDTO;
import com.getjavajob.training.web1702.balakine.dto.ChatMessageDTO;
import com.getjavajob.training.web1702.balakine.dto.ChatChannelDTO;
import com.getjavajob.training.web1702.balakine.model.Account;
import com.getjavajob.training.web1702.balakine.service.AccountService;
import com.getjavajob.training.web1702.balakine.service.MessageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.Collection;
import java.util.List;

import static com.getjavajob.training.web1702.balakine.service.Utils.AccountMapper.convertAccountToDTO;

@Controller
@SessionAttributes({"accountInSession", "base64Photo"})
public class MessagesController {

    private static final Logger logger = LoggerFactory.getLogger(MessagesController.class);
    private final MessageService messageService;
    private final AccountService accountService;

    @Autowired
    public MessagesController(MessageService messageService, AccountService accountService) {
        this.messageService = messageService;
        this.accountService = accountService;
    }


    @GetMapping(path = {"/account/messages/{id}", "/account/messages"})
    public ModelAndView viewMessagesOfId(@ModelAttribute("accountInSession") Account account,
                                         @PathVariable(name = "id", required = false) Long id) {

        ModelAndView modelAndView = new ModelAndView("/chat");
        if (id != null) {
            modelAndView.addObject("idFriend", id);
        }
        return modelAndView;

    }

    @PostMapping(value = "/account/getMessages", produces = "application/json;charset=UTF-8")
    @ResponseBody
    public Collection<ChatMessageDTO> getMessages(@ModelAttribute("accountInSession") Account account,
                                                  @RequestParam("idFriend") String id) {

        logger.debug("Start retrieving messages between " + account.getId() + " and " + id);

        List<ChatMessageDTO> messages = messageService.getAllMessagesWithFriend(account, Long.parseLong(id));

        logger.debug("messages found between " + account.getId() + " and " + id);
        return messages;
    }

    @PostMapping(value = "/account/getChatChannel")
    @ResponseBody
    public ChatChannelDTO initChatChannelDTO(@RequestParam("userOneId") String firstAccountId,
                                             @RequestParam("userTwoId") String secondAccountId) {

        InitChatChannelDTO initChatChannelDTO = InitChatChannelDTO.ChatChannelDtoBuilder.aChatChannelDto()
                .withUserOneId(Long.parseLong(firstAccountId))
                .withUserTwoId(Long.parseLong(secondAccountId))
                .build();
        logger.debug("Try to create new channel " + initChatChannelDTO);

        String channelUuid = messageService.initChatSession(initChatChannelDTO);

        AccountDTO firstAccount = convertAccountToDTO(accountService.read(initChatChannelDTO.getUserOneId()));
        AccountDTO secondAccount = convertAccountToDTO(accountService.read(initChatChannelDTO.getUserTwoId()));

        ChatChannelDTO chatChannelDTO = ChatChannelDTO.ChatChannelDTOBuilder.aChatChannelDTO()
                .withChannelUuid(channelUuid)
                .withFirstAccount(firstAccount)
                .withSecondAccount(secondAccount)
                .build();
        logger.debug("new chatChannel created: " + chatChannelDTO);
        return chatChannelDTO;




    }

    @MessageMapping(value = "/chat.sendMsg.{channelId}")
    @SendTo("/topic/chat.{channelId}")
    @ResponseBody
    public ChatMessageDTO sendMessage(@DestinationVariable String channelId,
                                      @RequestBody ChatMessageDTO chatMessageDTO) {



        logger.debug("New message with: " + chatMessageDTO);
        ChatMessageDTO chatMessageDTOSaved = messageService.addMessage(chatMessageDTO);
        logger.debug("message after save " + chatMessageDTOSaved);
        return chatMessageDTOSaved;


    }



}
