package com.getjavajob.training.web1702.balakine.webapp.controllers;

import com.getjavajob.training.web1702.balakine.dto.AccountDTO;
import com.getjavajob.training.web1702.balakine.service.AccountService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.*;

import static java.lang.Integer.parseInt;

@Controller
@SessionAttributes({"accountInSession", "base64Photo"})
public class SearchController {

    private static final Logger logger = LoggerFactory.getLogger(SearchController.class);

    private final AccountService accountService;

    @Autowired
    public SearchController(AccountService accountService) {
        this.accountService = accountService;
    }

    @GetMapping(value = "/account/allResults")
    public ModelAndView allResults(@RequestParam("filter") String filter) {


        ModelAndView modelAndView = new ModelAndView("/searchResults");

        modelAndView.addObject("foundedPages", accountService.searchByWord(filter));
        modelAndView.addObject("filter", filter);

        return modelAndView;
    }

    @GetMapping(value = "/account/countOfSearch")
    @ResponseBody
    public String getTotalCountOfUsers(@RequestParam("filter") String filter) {

        return String.valueOf(accountService.getTotalNumberFoundedEntities(filter));
    }

    @GetMapping(value = "/account/searchProgress")
    @ResponseBody
    public Collection<AccountDTO> getAccountsByWord(@RequestParam("filter") String filter,
                                                    @RequestParam(value = "page", required = false, defaultValue = "1") String page,
                                                    @RequestParam(value = "size", required = false, defaultValue = "4") String size) {

        logger.debug("Start find account with filter: " + filter);
        List<AccountDTO> accounts = accountService.searchByWord(parseInt(page), parseInt(size), filter);
        logger.debug("Accounts founded");
        return accounts;
    }
}


