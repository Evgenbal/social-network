package com.getjavajob.training.web1702.balakine.webapp.controllers;

import com.getjavajob.training.web1702.balakine.dto.WallMessageDTO;
import com.getjavajob.training.web1702.balakine.model.Account;
import com.getjavajob.training.web1702.balakine.service.WallMessageService;
import com.getjavajob.training.web1702.balakine.webapp.utils.SecurityUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.List;

import static com.getjavajob.training.web1702.balakine.webapp.utils.SecurityUtil.getAccountFromSecurityContext;

@RestController
@RequestMapping(value = "/account/wallPosts/", produces = "application/json;charset=UTF-8")
public class WallMessageController {

    private static final Logger logger = LoggerFactory.getLogger(WallMessageController.class);
    private final WallMessageService wallMessageService;

    @Autowired
    public WallMessageController(WallMessageService wallMessageService) {
        this.wallMessageService = wallMessageService;
    }

    //Get posts for account
    @RequestMapping(method = RequestMethod.GET)
    public Collection<WallMessageDTO> getPosts() {
        Account currentUser = SecurityUtil.getAccountFromSecurityContext();
        long id = currentUser.getId();
        logger.debug("Get wall posts for account with ID: " + id);
        List<WallMessageDTO> wallMessageDTOS = wallMessageService.getAllMessages(id);
        logger.debug("Get wall posts for account with ID: " + id + "; Size: " + wallMessageDTOS.size());
        return wallMessageDTOS;
    }

    //Get posts for another page
    @RequestMapping(method = RequestMethod.GET, value = "{accountId}")
    public Collection<WallMessageDTO> getPosts(@PathVariable("accountId") Long id) {
        logger.debug("Get wall posts for account with ID: " + id);
        List<WallMessageDTO> wallMessageDTOS = wallMessageService.getAllMessages(id);
        logger.debug("Get wall posts for account with ID: " + id + "; Size: " + wallMessageDTOS.size());
        return wallMessageDTOS;
    }


    @RequestMapping(method = RequestMethod.POST)
    public WallMessageDTO addPost(@RequestParam("text") String text,
                                  @RequestParam("ownerId") Long ownerId) {
        Account currentUser = getAccountFromSecurityContext();

        logger.debug("Start save new message with owner " + ownerId + "; writer " + currentUser.getId());
        WallMessageDTO wallMessageDTO = wallMessageService.saveWallMessage(currentUser, ownerId, text);

        logger.debug("Saved message with owner " + ownerId + "; writer " + currentUser.getId());
        return wallMessageDTO;
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "{wallPostId}")
    public ResponseEntity<?> deletePost(@PathVariable("wallPostId") Long wallPostId) {

        logger.debug("Start delete message with post id: " + wallPostId);
        wallMessageService.delete(wallPostId);
        logger.debug("Post deleted with id: " + wallPostId);
        return ResponseEntity.ok().build();
    }
}
