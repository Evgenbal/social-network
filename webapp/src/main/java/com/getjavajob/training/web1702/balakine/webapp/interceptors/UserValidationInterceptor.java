/*
package com.getjavajob.training.web1702.balakine.webapp.interceptors;

import com.getjavajob.training.web1702.balakine.model.Account;
import com.getjavajob.training.web1702.balakine.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class UserValidationInterceptor extends HandlerInterceptorAdapter {

    @Autowired
    private AccountService accountAccountService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object o) throws Exception {
        if (request.getSession(false) != null && request.getSession().getAttribute("autorise") != null) {
            return true;
        }
        String email = null;
        String password = null;
        Long id = null;
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (Cookie item : cookies) {
                if (item.getName().equals("mail")) {
                    email = item.getValue();
                }
                if (item.getName().equals("password")) {
                    password = item.getValue();
                }
                if (item.getName().equals("id")) {
                    id = Long.parseLong(item.getValue());
                }
            }
        }


        if (email != null && password != null && id != null) {
            Account account = accountAccountService.read(id);
            if (account.getMail().equals(email) &&
                    account.getPassword().equals(password)) {
                response.sendRedirect("account");
            }
        } else {
            request.setAttribute("doNotLogin", true);
            request.getRequestDispatcher("/join").forward(request, response);
        }
        return false;
    }


    @Override
    public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) throws Exception {

    }
}
*/
