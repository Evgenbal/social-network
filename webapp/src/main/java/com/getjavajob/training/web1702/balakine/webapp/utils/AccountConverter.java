package com.getjavajob.training.web1702.balakine.webapp.utils;

import com.getjavajob.training.web1702.balakine.model.Account;
import com.getjavajob.training.web1702.balakine.model.Phone;
import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class AccountConverter implements Converter {
    @Override
    public void marshal(Object o, HierarchicalStreamWriter writer, MarshallingContext context) {
        Account account = (Account) o;


        writer.startNode("firstName");
        writer.setValue(account.getFirstName());
        writer.endNode();

        writer.startNode("secondName");
        writer.setValue(account.getSecondName());
        writer.endNode();
        writer.startNode("thirdName");
        writer.setValue(account.getThirdName());
        writer.endNode();

        writer.startNode("birthDay");
        writer.setValue(account.getBirthday().toString());
        writer.endNode();

        writer.startNode("homeAddress");
        writer.setValue(account.getHomeAddress());
        writer.endNode();

        writer.startNode("workAddress");
        writer.setValue(account.getWorkAddress());
        writer.endNode();

        writer.startNode("icq");
        writer.setValue(account.getIcq());
        writer.endNode();

        writer.startNode("skype");
        writer.setValue(account.getSkype());
        writer.endNode();

        writer.startNode("phones");
        for (Phone item : account.getPhones()) {
            context.convertAnother(item, new PhonesConverter());
        }
        writer.endNode();

    }

    @Override
    public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {
        Account account = Account.AccountBuilder.anAccount().build();


        reader.moveDown();
        account.setFirstName(reader.getValue());
        reader.moveUp();

        reader.moveDown();
        account.setSecondName(reader.getValue());
        reader.moveUp();

        reader.moveDown();
        account.setThirdName(reader.getValue());
        reader.moveUp();

        reader.moveDown();
        account.setBirthday(LocalDate.parse(reader.getValue()));
        reader.moveUp();

        reader.moveDown();
        account.setHomeAddress(reader.getValue());
        reader.moveUp();

        reader.moveDown();
        account.setWorkAddress(reader.getValue());
        reader.moveUp();

        reader.moveDown();
        account.setIcq(reader.getValue());
        reader.moveUp();

        reader.moveDown();
        account.setSkype(reader.getValue());
        reader.moveUp();

        List<Phone> phones = new ArrayList<>();


        reader.moveDown();
        while (reader.hasMoreChildren()) {
            phones.add((Phone) context.convertAnother(account, Phone.class, new PhonesConverter()));
        }
        reader.moveUp();

        account.setPhones(phones);

        return account;
    }

    @Override
    public boolean canConvert(Class aClass) {
        return aClass.equals(Account.class);
    }
}
