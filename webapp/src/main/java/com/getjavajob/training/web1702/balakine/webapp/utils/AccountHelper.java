package com.getjavajob.training.web1702.balakine.webapp.utils;

import com.getjavajob.training.web1702.balakine.model.Account;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.servlet.ModelAndView;

import static com.getjavajob.training.web1702.balakine.service.Utils.AccountPhotoConverter.getBase64Photo;
import static com.getjavajob.training.web1702.balakine.service.Utils.AccountPhotoConverter.getPhotoByteArray;

public class AccountHelper {
    public static ModelAndView setAttributes(ModelAndView modelAndView, Account account) {
        return modelAndView
                .addObject("accountInSession", account)
                .addObject("base64Photo", getBase64Photo(getPhotoByteArray(account)));
    }

}
