package com.getjavajob.training.web1702.balakine.webapp.utils;

import com.getjavajob.training.web1702.balakine.model.Phone;
import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

public class PhonesConverter implements Converter {
    @Override
    public void marshal(Object o, HierarchicalStreamWriter writer, MarshallingContext marshallingContext) {
        Phone phone = (Phone) o;
        writer.startNode("phone");

        writer.startNode("number");
        writer.setValue(phone.getPhoneNumber());
        writer.endNode();

        writer.endNode();

    }

    @Override
    public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext unmarshallingContext) {
        Phone phone = Phone.PhoneBuilder.aPhone().build();

        reader.moveDown();

        reader.moveDown();
        phone.setPhoneNumber(reader.getValue());
        reader.moveUp();

        reader.moveUp();

        return phone;
    }

    @Override
    public boolean canConvert(Class aClass) {
        return aClass.equals(Phone.class);
    }

}
