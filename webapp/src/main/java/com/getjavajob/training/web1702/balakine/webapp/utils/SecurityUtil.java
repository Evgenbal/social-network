package com.getjavajob.training.web1702.balakine.webapp.utils;

import com.getjavajob.training.web1702.balakine.model.Account;
import com.getjavajob.training.web1702.balakine.utils.CustomUserDetails;
import com.getjavajob.training.web1702.balakine.webapp.controllers.MainController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

public class SecurityUtil {

    private static final Logger logger = LoggerFactory.getLogger(SecurityUtil.class);

    public static Account getAccountFromSecurityContext() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        logger.debug("Authenticated " + authentication.isAuthenticated());

        return authentication instanceof AnonymousAuthenticationToken ?
                null : ((CustomUserDetails) authentication.getPrincipal()).getAccount();
    }


    public static boolean isCurrentAuthenticated() {
        return SecurityContextHolder.getContext().getAuthentication().isAuthenticated();

    }
}
