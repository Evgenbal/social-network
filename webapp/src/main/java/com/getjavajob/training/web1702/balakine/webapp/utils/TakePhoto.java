package com.getjavajob.training.web1702.balakine.webapp.utils;

import com.getjavajob.training.web1702.balakine.model.AbstractMainModel;
import com.getjavajob.training.web1702.balakine.model.AbstractModel;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.Base64;

public class TakePhoto {
    public static String getBase64Photo(byte[] photo) {
        return Base64.getEncoder().encodeToString(photo);
    }

    public static <T extends AbstractMainModel> byte[] getPhotoByteArray(T obj) {
        if (obj.getPhoto() == null) {
            ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
            File photo = new File(classLoader.getResource("default.jpg").getFile());
            byte[] photoByteArray = new byte[0];
            try {
                photoByteArray = Files.readAllBytes(photo.toPath());
            } catch (IOException e) {
                e.printStackTrace();
            }
            return photoByteArray;
        } else {
            return obj.getPhoto();
        }

    }
}
