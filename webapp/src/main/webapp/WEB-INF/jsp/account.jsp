<%@ page import="com.getjavajob.training.web1702.balakine.model.Account" %><%--
  Created by IntelliJ IDEA.
  User: mac
  Date: 12/05/17
  Time: 20:55
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <head>
        <jsp:include page="header.jsp"/>
        <jsp:include page="sidebar.jsp"/>

        <!--CSS-->

        <link rel='stylesheet' href='${pageContext.request.contextPath}/resources/css/account.css' type='text/css'
              media='all'>

        <!--JS-->

        <script src="${pageContext.request.contextPath}/resources/js/account.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/resources/js/collapseInfo.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/resources/js/formatWallPost.js" type="text/javascript"></script>

    </head>
    <body>
        <div hidden id="contextPath">${pageContext.request.contextPath}</div>
        <div hidden id="accountId">${accountInSession.accountId}</div>
        <div class="col-md-2">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="media">
                        <div align="center">
                            <img class="thumbnail img-responsive"
                                 src="data:image/jpeg;base64,${base64Photo}" width="300px"
                                 height="300px">
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <div class="panel panel-default">
                <div class="panel-heading">Friends</div>
                <div class="panel-body">
                    <div id="accountFriends">

                    </div>
                </div>
            </div>
            <br>
        </div>
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="panel-title pull-left" style="font-size:30px;">
                        <c:out value=" ${accountInSession.firstName} ${accountInSession.secondName}">
                        </c:out>
                        <small>${friendRequester.thirdName}</small>

                    </div>
                    <br>
                    <br>
                    <hr>
                    <br>
                    <button id="showInfoButton" type="button" data-toggle="collapse" data-target="#privateInfo"
                            class="btn btn-primary">
                        Show full info
                    </button>
                    <br>
                    <br>
                    <div id="privateInfo" class="collapse customCollapse">
                        <table class="table table-user-information">
                            <tbody>
                                <tr>
                                    <td>Birthday:</td>
                                    <td><c:out value="${accountInSession.birthday}"/></td>

                                </tr>
                                <tr>
                                    <td>Home address:</td>
                                    <td><c:out value="${accountInSession.homeAddress}"/></td>
                                </tr>
                                <tr>
                                    <td>Work address:</td>
                                    <td><c:out value="${accountInSession.workAddress}"/></td>
                                </tr>
                                <tr>
                                    <td>Skype:</td>
                                    <td><c:out value="${accountInSession.skype}"/></td>
                                </tr>
                                <tr>
                                    <td>Icq:</td>
                                    <td><c:out value="${accountInSession.icq}"/></td>
                                </tr>
                                <c:forEach var="item" items="${accountInSession.phones}">
                                    <tr>
                                        <c:if test="${not empty item.phoneNumber}">
                                            <td>Phone number:</td>
                                            <td><c:out value="${item.phoneNumber}"/></td>
                                        </c:if>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <br>

            <div class="panel panel-default">
                <div class="panel-body">
                    <form class="form-horizontal" role="form" id="newWallMessage" name="newWallMessage">
                        <input type="text" placeholder="Type new post" id="newWallMessageInput">
                        <button type="submit" class="btn btn-success">
                            <span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Add post
                        </button>
                    </form>

                </div>
            </div>
            </form>
            <div id="wallPosts">
                <%--<c:forEach var="i" items="${requestScope.wallPosts}">
                    <div class="panel panel-default" id="wallPostId${i.messageId}">
                        <div class="panel-body">
                            <div class="pull-left">
                                <a href="${pageContext.request.contextPath}/id${i.senderId}">
                                    <img class="media-object img-circle" src="data:image/jpeg;base64,${i.senderPhoto}"
                                         width="50px"
                                         height="50px" style="margin-right:8px; margin-top:-5px;">
                                </a>
                            </div>
                            <h4>
                                <a href="${pageContext.request.contextPath}/id${i.senderId}"
                                   style="text-decoration:none;">
                                    <strong>${i.senderFirstName} ${i.senderSecondName} </strong></a> –
                                <small>
                                    <small>
                                        <a href="${pageContext.request.contextPath}/id${i.senderId}"
                                           style="text-decoration:none; color:grey;"><i><i class="fa fa-clock-o"
                                                                                           aria-hidden="true">

                                        </i> ${i.postTime}</i></a>
                                    </small>
                                </small>
                            </h4>
                            <div class="navbar-right">
                                <div class="dropdown">
                                    <button class="glyphicon glyphicon-remove deletePostBtn" type="button"
                                            id="${i.messageId}" >
                                    </button>
                                </div>
                            </div>
                            <hr>
                            <div class="post-content">
                                    ${i.text}
                            </div>
                            <hr>
                        </div>
                    </div>
                </c:forEach>--%>
            </div>
        </div>

    </body>
</html>

