<%--
  Created by IntelliJ IDEA.
  User: mac
  Date: 24/10/17
  Time: 18:32
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <%@include file="header.jsp" %>
        <%@include file="sidebar.jsp" %>

        <!--CSS-->

        <link rel='stylesheet' href='${pageContext.request.contextPath}/resources/css/chat.css'
              type='text/css'
              media='all'>

        <!--JS-->

        <script src="${pageContext.request.contextPath}/resources/js/chat.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/webjars/sockjs-client/1.1.2/sockjs.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/webjars/stomp-websocket/2.3.3-1/stomp.js" type="text/javascript"></script>
    </head>
    <body>

        <div hidden id="contextPath">${pageContext.request.contextPath}</div>
        <div class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-heading">Messages</div>
                <div class="panel-body">
                    <div id="messagesWithFriend">


                    </div>
                </div>
            </div>
        </div>
        </div>
        <div class="col-md-6 frame ">
            <div hidden id="accountId">${sessionScope.accountInSession.accountId}</div>
            <ul class="messageText">

            </ul>
            <div>
                <div class="msj-rta macro" style="margin:auto">
                    <div class="text text-r" style="background:whitesmoke !important">
                        <input class="mytext" placeholder="Type a messageText"/>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
