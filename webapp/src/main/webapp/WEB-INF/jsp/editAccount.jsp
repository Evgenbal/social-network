<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%--
  Created by IntelliJ IDEA.
  User: mac
  Date: 12/05/17
  Time: 20:56
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
    <head>

        <%--
                <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
        --%>

        <%@include file="header.jsp" %>
        <%@include file="sidebar.jsp" %>
        <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.10/jquery.mask.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/editAccount.js" type="text/javascript"></script>

        <link rel='stylesheet' href='${pageContext.request.contextPath}/resources/css/editAccount.css' type='text/css'
              media='all'>

    </head>
    <body>
        <div class="hidden" id="contextPath">${pageContext.request.contextPath}</div>
        <div class="modal fade" id="changeXml" tabindex="-1" role="dialog" aria-labelledby="changeXmlLabel"
             aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="changeXmlLabel">Upload XML</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <form class="form-horizontal" role="form"
                          enctype="multipart/form-data"
                          name="loadAccountFromXml" id="loadAccountFromXml">
                        <div class="modal-body">

                            <div class="form-group">
                                <input type="file" class="custom-file-input" id="xmlFile" name="xmlFile">
                                <span class="custom-file-control"></span>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn btn-success">OK</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="col-md-10">
            <div role="tabpanel">
                <ul class="nav nav-tabs" id="tabsEdit">
                    <li>
                        <a href="#mainInfo" data-toggle="tab">
                            General
                        </a>
                    </li>
                    <li>
                        <a href="#securityInfo" data-toggle="tab">
                            Security
                        </a>
                    </li>
                </ul>
                <div id="infoMessage">

                </div>

                <div class="tab-content">
                    <div class="tab-pane active" id="mainInfo">
                        <hr>
                        <div class="row">
                            <form class="form-horizontal" role="form" id="formMainInfo" name="formMainInfo">
                                <div class="col-md-3">
                                    <div class="text-center">
                                        <img src="data:image/jpeg;base64,<c:out value="${sessionScope.base64Photo}"/>"
                                             class="img-circle"
                                             width="300px"
                                             height="300px">

                                        <div class="form-group">
                                            <label class="custom-file" for="newPhoto">Upload new profile photo</label>
                                            <input type="file" class="custom-file-input" name="newPhoto"
                                                   id="newPhoto">
                                            <span class="custom-file-control"></span>

                                        </div>

                                        <div class="form-check">
                                            <label class="form-check-label">
                                                <input type="checkbox" class="form-check-input" name="deletePhoto">
                                                Delete photo
                                            </label>
                                        </div>

                                        <hr>

                                        <div class="form-group">
                                            <button type="button" class="btn btn-success"
                                                    onClick='location.href="${pageContext.request.contextPath}/account/downloadAccount"'>
                                                Download info in XML
                                            </button>
                                        </div>

                                        <div class="form-group">
                                            <button type="button"
                                                    class="btn btn-primary"
                                                    data-toggle="modal"
                                                    data-target="#changeXml">
                                                Upload XML
                                            </button>
                                        </div>
                                        <hr>
                                    </div>
                                </div>

                                <div class="col-md-9" id="mainInfoColumn">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Name:</label>
                                        <div class="col-md-8">
                                            <input class="form-control" type="text"
                                                   value="${accountInSession.firstName}" required
                                                   name="firstName">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">LastName:</label>
                                        <div class="col-md-8">
                                            <input class="form-control" type="text"
                                                   value="${accountInSession.secondName}" required
                                                   name="secondName">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Patronymic:</label>
                                        <div class="col-md-8">
                                            <input class="form-control" type="text"
                                                   value="${accountInSession.thirdName}"
                                                   name="thirdName">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">BirthDay:</label>
                                        <div class="col-md-8">
                                            <input class="form-control birthdayMask" type="text"
                                                   value="${accountInSession.birthday}"
                                                   placeholder="ГГГГ-ММ-ДД"
                                                   name="birthday">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Home address:</label>
                                        <div class="col-md-8">
                                            <input class="form-control" type="text"
                                                   value="${accountInSession.homeAddress}"
                                                   name="homeAddress">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Work address:</label>
                                        <div class="col-md-8">
                                            <input class="form-control" type="text"
                                                   value="${accountInSession.workAddress}"
                                                   name="workAddress">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">ICQ:</label>
                                        <div class="col-md-8">
                                            <input class="form-control" type="text"
                                                   value="${accountInSession.icq}" name="icq">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Skype:</label>
                                        <div class="col-md-8">
                                            <input class="form-control" type="text"
                                                   value="${accountInSession.skype}" name="skype">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label"></label>
                                        <div class="col-md-8">
                                            <button class="btn btn-primary btn-xs" type="button" id="addRow">
                                                Add another phone
                                            </button>
                                        </div>
                                    </div>

                                    <div id="fieldOfPhones">
                                        <c:set var="count" value="0" scope="page"/>
                                        <c:set var="user" value="${accountInSession.accountId}"
                                               scope="page"/>
                                        <c:forEach var="item"
                                                   items="${accountInSession.phones}"
                                                   varStatus="itemIndex">
                                            <c:set var="count" value="${count+1}" scope="page"/>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Phone number:</label>

                                                <div class="col-md-7">


                                                    <input class="form-control phoneMask" type="text"
                                                           value="${item.phoneNumber} "
                                                           name="phones[${itemIndex.index}].phoneNumber">

                                                    <input type="hidden" value="${item.phoneId}"
                                                           name="phones[${itemIndex.index}].phoneId">


                                                    <input type="hidden" value="${user}"
                                                           name="phones[${itemIndex.index}].user.accountId">
                                                </div>
                                                <div class="col-md-1">
                                                    <input type="button" value="-" onclick="removeRow(this)">
                                                </div>
                                            </div>
                                        </c:forEach>
                                        <div hidden id="userName">${user}</div>
                                        <div hidden id="count">${count}</div>
                                    </div>
                                    <div class="form-group" id="saveChanges">
                                        <label class="col-md-3 control-label"></label>
                                        <div class="col-md-9">
                                            <button type="submit" class="btn btn-primary" data-toggle="modal" id="formDone">
                                                Save
                                            </button>
                                            <input type="reset" class="btn btn-default" value="Cancel">
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="tab-pane " id="securityInfo">
                        <hr>
                        <div class="row">
                            <!-- left column -->
                            <form class="form-horizontal" role="form" method=post
                                  enctype="multipart/form-data" id="formSecurityInfo" name="accountInSessionSecurity">
                                <div class="form-group" id="mailForJs">
                                    <label class="col-md-4 control-label">Mail:</label>
                                    <div class="col-md-7">
                                        <input class="form-control" type="text" id="editedMail"
                                               value="${accountInSession.mail}" name="mail">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">New password:</label>
                                    <div class="col-md-7">
                                        <input class="form-control" type="password"
                                               name="newPassword" id="newPassword">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Repeat new password:</label>
                                    <div class="col-md-7">
                                        <input class="form-control" type="password"
                                               name="controlPassword" id="controlPassword">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Current password:</label>
                                    <div class="col-md-7">
                                        <input class="form-control" type="password" id="oldPassword"
                                               name="oldPassword" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label"></label>
                                    <div class="col-md-7">
                                        <button id="saveChangesSecurity" type="button" class="btn btn-primary">
                                            Save
                                        </button>
                                        <span></span>
                                        <input type="reset" class="btn btn-default" value="Cancel">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
