<%@ page import="com.getjavajob.training.web1702.balakine.model.Account" %><%--
  Created by IntelliJ IDEA.
  User: mac
  Date: 12/05/17
  Time: 20:55
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
    <head>
        <%@include file="header.jsp" %>
        <%@include file="sidebar.jsp" %>

        <!--CSS-->

        <link rel='stylesheet' href='${pageContext.request.contextPath}/resources/css/account.css' type='text/css'
              media='all'>

        <!--JS-->

        <script src="${pageContext.request.contextPath}/resources/js/friends.js" type="text/javascript"></script>

    </head>
    <body>
        <div class="col-md-10">
            <div role="tabpanel">
                <ul class="nav nav-tabs" id="tabsEdit">
                    <li>
                        <a href="#friends" data-toggle="tab">
                            Friends
                        </a>
                    </li>
                    <li>
                        <a href="#requests" data-toggle="tab">
                            New friends
                        </a>
                    </li>
                    <li>
                        <a href="#myRequests" data-toggle="tab">
                            Outgoing
                        </a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="friends">
                        <hr>
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <c:forEach var="i" items="${requestScope.friendsMap}">

                                    <div class="pull-left">
                                        <a href="${pageContext.request.contextPath}/id${i.accountId}">
                                            <img class="media-object img-circle"
                                                 src="data:image/jpeg;base64,${i.photo}"
                                                 width="75px" height="75px" style="margin-right:8px; margin-top:-5px;">
                                        </a>
                                    </div>
                                    <h4><a href="${pageContext.request.contextPath}/id${i.accountId}"
                                           style="text-decoration:none;"><strong>${i.firstName} ${i.secondName}</strong></a>
                                    </h4>
                                    <div class="pull-right">
                                        <button type="button" class="btn btn-success"
                                                onclick='location.href = "${pageContext.request.contextPath}/account/messages/${i.accountId}"'>
                                            <span class="glyphicon glyphicon-envelope" aria-hidden="true"></span>
                                            Write a message
                                        </button>
                                    </div>
                                    <br>
                                    <hr>
                                    <br>

                                </c:forEach>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane " id="requests">
                        <hr>
                        <!--todo-->
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <c:forEach var="i" items="${requestScope.requestsMap}">

                                    <div class="pull-left">
                                        <a href="${pageContext.request.contextPath}/id${i.accountId}">
                                            <img class="media-object img-circle"
                                                 src="data:image/jpeg;base64,${i.photo}"
                                                 width="75px" height="75px" style="margin-right:8px; margin-top:-5px;">
                                        </a>
                                    </div>
                                    <h4><a href="${pageContext.request.contextPath}/id${i.accountId}"
                                           style="text-decoration:none;"><strong>${i.firstName} ${i.secondName}</strong></a>
                                    </h4>
                                    <br>
                                    <hr>
                                    <br>
                                    <%--<h4><a href="${pageContext.request.contextPath}/id${i.friendRequester.accountId}"
                                           style="text-decoration:none;"><strong>${i.friendRequester.firstName} ${i.friendRequester.secondName}</strong></a>
                                    </h4>--%>
                                </c:forEach>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane " id="myRequests">
                        <hr>
                        <!--todo-->
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <c:forEach var="i" items="${requestScope.outgoingMap}">

                                    <div class="pull-left">
                                        <a href="${pageContext.request.contextPath}/id${i.accountId}">
                                            <img class="media-object img-circle"
                                                 src="data:image/jpeg;base64,${i.photo}"
                                                 width="75px" height="75px" style="margin-right:8px; margin-top:-5px;">
                                        </a>
                                    </div>
                                    <h4><a href="${pageContext.request.contextPath}/id${i.accountId}"
                                           style="text-decoration:none;"><strong>${i.firstName} ${i.secondName}</strong></a>
                                    </h4>
                                    <br>
                                    <hr>
                                    <br>
                                </c:forEach>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>