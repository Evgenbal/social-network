<%--
  Created by IntelliJ IDEA.
  User: mac
  Date: 24/05/17
  Time: 15:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<html>
    <head>
        <!-- CSS-->


        <link rel="stylesheet" href="${pageContext.request.contextPath}/webjars/bootstrap/3.3.7/css/bootstrap.css"
              type="text/css" media="all"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/webjars/jquery-ui/1.12.1/jquery-ui.css"
              type="text/css" media="all"/>

        <link rel='stylesheet' href='${pageContext.request.contextPath}/resources/css/header.css' type='text/css'
              media='all'>
        <!--JS-->

        <script src="${pageContext.request.contextPath}/webjars/jquery/3.3.1/jquery.min.js"
                type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/webjars/jquery-ui/1.12.1/jquery-ui.min.js"
                type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/webjars/jquery.inputmask/3.1.0/jquery.inputmask.bundle.min.js"
                type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/webjars/bootstrap/3.3.7/js/bootstrap.min.js"
                type="text/javascript"></script>

        <script src="${pageContext.request.contextPath}/resources/js/header.js" type="text/javascript"></script>

    </head>
    <body>
        <nav class="nav navbar-default navbarStyle">
            <div class="container-fluid">
                <p class="navbar-text "> FaceBooKiller</p>
                <c:if test="${sessionScope.accountInSession!=null}">
                    <form class="navbar-form navbar-left" method="get"
                          action="${pageContext.request.contextPath}/account/">

                        <div class="searchField input-group">
                            <input type="text" class="search form-control" placeholder="Поиск" name="filter"
                                   id="filterInHeader">
                            <div class="input-group-btn">
                                <button class="btn btn-default" type="submit">
                                    <i class="glyphicon glyphicon-search"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                    <ul class=" nav navbar-nav navbar-right">
                        <li><a href="${pageContext.request.contextPath}/logout" class="logout"><span
                                class="glyphicon glyphicon-log-out logoutIcon"></span> Logout</a></li>
                    </ul>
                </c:if>
                <c:if test="${sessionScope.accountInSession==null}">
                    <ul class=" nav navbar-nav navbar-right">
                        <li><a href="${pageContext.request.contextPath}/login"><span
                                class="glyphicon glyphicon-log-in logoutIcon"></span> Login</a></li>
                    </ul>
                </c:if>
            </div>
        </nav>
    </body>
</html>
