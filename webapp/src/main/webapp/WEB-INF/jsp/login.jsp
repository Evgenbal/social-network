<%--
  Created by IntelliJ IDEA.
  User: mac
  Date: 22/05/17
  Time: 17:34
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <head>
        <%@include file="header.jsp" %>

        <link rel='stylesheet' href='${pageContext.request.contextPath}/resources/css/login.css' type='text/css'
              media='all'>

        <script src="${pageContext.request.contextPath}/resources/js/login.js" type="text/javascript"></script>

    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="panel panel-login">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                        <c:if test="${not empty error}">
                                            <div class="error alert alert-danger">
                                                <span class=" glyphicon glyphicon-remove"></span>
                                                ${error}</div>
                                        </c:if>
                                        <c:if test="${not empty msg}">
                                            <div class="msg alert alert-success">
                                                <span class="glyphicon glyphicon-ok-circle"></span>
                                                    ${msg}
                                            </div>
                                        </c:if>
                                    <form id="login-form"
                                          action="${pageContext.request.contextPath}/loginValid" method="post"
                                          style="display: block;">

                                        <div class="form-group">
                                            <input type="text" name="mail" id="mail" tabindex="1"
                                                   class="form-control user" placeholder="Mail" value="">
                                        </div>
                                        <div class="form-group">
                                            <input type="password" name="password" id="password" tabindex="2"
                                                   class="form-control password" placeholder="Password">
                                        </div>
                                        <div class="form-group text-center">
                                            <input type="checkbox" tabindex="3" class="" name="remember-me"
                                                   id="remember-me">
                                            <label for="remember-me"> Remember me</label>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-sm-6 col-sm-offset-3">
                                                    <input type="button" name="login-submit" id="login-submit"
                                                           tabindex="4" class="form-control btn btn-login"
                                                           value="Log In">
                                                    <input type="submit" id="login-submit-forJS" hidden>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="text-center">
                                                        <a href="forgot" tabindex="5" class="forgot-password">Forgot
                                                            your password?</a>

                                                        <a href="registration" tabindex="5" class="forgot-password">Registration</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>