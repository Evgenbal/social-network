<%--
  Created by IntelliJ IDEA.
  User: mac
  Date: 24/10/17
  Time: 20:19
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <%@include file="header.jsp" %>
        <%@include file="sidebar.jsp" %>

        <!--CSS-->

        <link rel='stylesheet' href='${pageContext.request.contextPath}/resources/css/messagesAll.css' type='text/css'
              media='all'>

        <!--JS-->

    </head>
    <body>
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="panel-title pull-left" style="font-size:30px;"> Сообщения
                    </div>
                    <br>
                    <c:forEach var="i" items="${accountWithLastMessages}">
                        <div class="pull-left">
                            <a href="${pageContext.request.contextPath}/messageText/id${i.key.id}">
                                <img class="media-object img-circle"
                                     src="data:image/jpeg;base64,${i.value}"
                                     width="75px" height="75px" style="margin-right:8px; margin-top:-5px;">
                            </a>
                        </div>
                        <h4><a href="${pageContext.request.contextPath}/messageText/id${i.key.id}"
                               style="text-decoration:none;"><strong>${i.key.firstName} ${i.key.secondName}</strong></a>
                        </h4>
                        <br>
                        <hr>
                        <br>
                    </c:forEach>
                </div>
            </div>
        </div>
    </body>
</html>
