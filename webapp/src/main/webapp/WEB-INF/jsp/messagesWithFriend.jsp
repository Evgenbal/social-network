<%--
  Created by IntelliJ IDEA.
  User: mac
  Date: 24/10/17
  Time: 18:32
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <%@include file="header.jsp" %>
        <%@include file="sidebar.jsp" %>

        <!--CSS-->

        <link rel='stylesheet' href='${pageContext.request.contextPath}/resources/css/messagesWithFriend.css'
              type='text/css'
              media='all'>

        <!--JS-->

        <script src="${pageContext.request.contextPath}/resources/js/messagesWithFriend.js"
                type="text/javascript"></script>
    </head>
    <body>

        <div hidden id="contextPath">${pageContext.request.contextPath}</div>
        <div class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-heading">Messages</div>
                <div class="panel-body">
                    <div id="messagesWithFriend">


                    </div>
                </div>
            </div>
        </div>
        </div>
        <div class="col-md-6 frame ">
            <div hidden id="idFriend">${idFriend}</div>
            <div hidden id="photoAccount">${sessionScope.base64Photo}</div>
            <div hidden id="accountName">${sessionScope.accountInSession.firstName}</div>
            <ul class="messages">

            </ul>
            <div>
                <div class="msj-rta macro" style="margin:auto">
                    <div class="text text-r" style="background:whitesmoke !important">
                        <input class="mytext" placeholder="Type a message"/>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
