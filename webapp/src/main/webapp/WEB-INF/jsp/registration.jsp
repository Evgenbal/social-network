<%--
  Created by IntelliJ IDEA.
  User: mac
  Date: 12/05/17
  Time: 20:57
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <%@include file="header.jsp" %>
        <%--
                <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
        --%>
        <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.10/jquery.mask.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/registration.js" type="text/javascript"></script>

        <link rel='stylesheet' href='${pageContext.request.contextPath}/resources/css/registration.css' type='text/css'
              media='all'>
    </head>
    <body>
        <div style="padding-top: 40px;"></div>
        <div class="container">
            <div id="infoMessage">

            </div>
            <form class="form-horizontal" method=post action="${pageContext.request.contextPath}/registration"
                   id="registrationForm">
                <div class="form-group">
                    <label class="col-md-4 control-label">Name:</label>
                    <div class="col-md-7">
                        <input class="form-control" type="text"
                               required
                               name="firstName">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label">Surname:</label>
                    <div class="col-md-7">
                        <input class="form-control" type="text"
                               required
                               name="secondName">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label">Mail</label>
                    <div class="col-md-7">
                        <input class="form-control" type="text"
                               name="mail" id="mail" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label">Password:</label>
                    <div class="col-md-7">
                        <input class="form-control" type="password"
                               name="password" id="password" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label">Repeat password:</label>
                    <div class="col-md-7">
                        <input class="form-control" type="password"
                               name="passwordTest" id="passwordTest" required>
                    </div>
                </div>

                <div class="form-group" id="saveChanges">
                    <label class="col-md-4 control-label"></label>
                    <div class="col-md-7">
                        <button type="submit" class="btn btn-primary" id="registrationButton">Registration
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </body>
</html>
