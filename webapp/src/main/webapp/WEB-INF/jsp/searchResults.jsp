<%--
  Created by IntelliJ IDEA.
  User: mac
  Date: 24/05/17
  Time: 21:24
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <head>
        <%@include file="header.jsp" %>
        <%@include file="sidebar.jsp" %>

        <link rel='stylesheet' href='${pageContext.request.contextPath}/resources/css/searchResults.css' type='text/css'
              media='all'>

        <script src="${pageContext.request.contextPath}/resources/js/searchResults.js" type="text/javascript"></script>


    </head>
    <body>
        <div hidden id="contextPath">${pageContext.request.contextPath}</div>

        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <span>
                        <h4 class="panel-title pull-left" style="font-size:30px;">
                            Search results:
                        </h4>
                    </span>
                    <br>
                    <br>
                    <hr>
                    <br>
                    <div class=" input-group">
                        <div class="form-group has-feedback has-feedback-left">
                            <input type="text" class=" form-control"
                                   placeholder="Start to enter name" name="filter"
                                   id="filter" value=${filter}>
                            <div class="input-group-btn">
                                <button class="btn btn-default" type="submit">
                                    <i class="glyphicon glyphicon-search"></i>
                                </button>
                            </div>
                            <i class="glyphicon glyphicon-search form-control-feedback"></i>
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-body">
                    <div id="accountList">
                        <c:forEach var="i" items="${requestScope.foundedPages}">
                            <div class="pull-left">
                                <a href="${pageContext.request.contextPath}/id${i.key.id}">
                                    <img class="media-object img-circle"
                                         src="data:image/jpeg;base64,${i.value}"
                                         width="75px" height="75px" style="margin-right:8px; margin-top:-5px;">
                                </a>
                            </div>
                            <h4><a href="${pageContext.request.contextPath}/id${i.key.id}"
                                   style="text-decoration:none;"><strong>${i.key.firstName} ${i.key.secondName}</strong></a>
                            </h4>
                            <br>
                            <hr>
                            <br>
                        </c:forEach>

                    </div>
                </div>
            </div>
            <div id="test">

            </div>
            <ul class="pagination  justify-content-end">
                <li class="page-item">
                    <a id="first" class="page">
                        1
                    </a>
                </li>

                <li class="page-item disabled">
                    <span id="passAtBegin" class="pass" >
                        ...
                    </span>
                </li>
                <li class="page-item">
                    <a id="currentMinusOne" class="page">

                    </a>
                </li>
                <li class="page-item active">
                    <a href="#" id="current" class="page">
                        1
                    </a>
                </li>
                <li class="page-item">
                    <a href="#" id="currentPlusOne" class="page">

                    </a>
                </li>
                <li class="page-item disabled">
                    <span id="passInEnd" class="pass">
                        ...
                    </span>
                </li>

                <li class="page-item">
                    <a  href="#" id="last" class="page">

                    </a>
                </li>
            </ul>
        </div>
        <%--
               <div class="panel panel-default">
                   <div class="panel-body">

                   </div>
               </div>
           </c:forEach>--%>
    </body>
</html>
