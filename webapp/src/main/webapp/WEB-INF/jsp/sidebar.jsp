<%--
  Created by IntelliJ IDEA.
  User: mac
  Date: 07/07/17
  Time: 19:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
    <head>
        <!-- CSS-->

        <link rel="stylesheet" href="${pageContext.request.contextPath}/webjars/bootstrap/3.3.7/css/bootstrap.css"
              media="all"/>


        <link rel='stylesheet' href='${pageContext.request.contextPath}/resources/css/sidebar.css' type='text/css'
              media='all'>

        <!--JS-->

    </head>
    <body>


        <div class="col-md-2">
            <c:if test="${sessionScope.accountInSession!=null}">
                <nav class="nav-sidebar">
                    <ul class="nav">
                        <li class="hz"><a href="${pageContext.request.contextPath}/account"><span
                                class="glyphicon glyphicon-user"></span>My Profile</a>
                            <a href="${pageContext.request.contextPath}/account/editAccount"><span
                                    class="glyphicon glyphicon-cog"></span> </a>
                        </li>
                    </ul>
                    <ul class="nav">
                        <li class="hz">
                            <a href="${pageContext.request.contextPath}/account/friends"><span
                                    class="badge">${sessionScope.newFriends}</span>Friends</a>
                        </li>
                    </ul>
                    <ul class="nav">
                        <li class="hz"><a href="${pageContext.request.contextPath}/account/messages"><span
                                class="badge">${sessionScope.newMessages}</span> Messages</a>
                        </li>
                    </ul>
                </nav>
            </c:if>
        </div>

    </body>
</html>
