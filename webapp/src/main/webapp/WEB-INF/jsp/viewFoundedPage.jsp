<%@ page import="com.getjavajob.training.web1702.balakine.model.Account" %><%--
  Created by IntelliJ IDEA.
  User: mac
  Date: 12/05/17
  Time: 20:55
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <head>
        <%@include file="header.jsp" %>
        <%@include file="sidebar.jsp" %>

        <!--CSS-->

        <link rel='stylesheet' href='${pageContext.request.contextPath}/resources/css/viewFoundedPage.css'
              type='text/css'
              media='all'>

        <!--JS-->

        <script src="${pageContext.request.contextPath}/resources/js/collapseInfo.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/resources/js/changeFriendButton.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/resources/js/viewFoundedPage.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/resources/js/formatWallPost.js" type="text/javascript"></script>


    </head>
    <body>
        <div id="accountId" hidden>${requestScope.foundedPage.accountId}</div>
        <div id="contextPath" hidden>${pageContext.request.contextPath}</div>
        <div class="col-md-2 ">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="media">
                        <div align="center">
                            <img class="thumbnail img-responsive"
                                 src="data:image/jpeg;base64,<c:out value="${pagePhoto}"/>" width="300px"
                                 height="300px">
                        </div>
                    </div>
                    <br>
                </div>
            </div>

            <div id="buttonParent">
                <!-- Buttons In JS ---->
            </div>
            <br>
            <br>
            <br>
            <div class="panel panel-default">
                <div class="panel-heading">Friends</div>
                <div class="panel-body">
                    <div id="accountFriends">
                        <!-- List In JS ---->
                    </div>
                </div>
            </div>
            <br>
        </div>
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="panel-title pull-left" style="font-size:30px;">
                        <c:out value=" ${foundedPage.firstName} ${foundedPage.secondName}">
                        </c:out>
                        <small>${sessionScope.friendRequester.thirdName}</small>

                    </div>
                    <br>
                    <br>
                    <hr>
                    <br>
                    <button id="showInfoButton" type="button" data-toggle="collapse" data-target="#privateInfo"
                            class="btn btn-primary">
                        Show full info
                    </button>
                    <br>
                    <br>
                    <div id="privateInfo" class="collapse customCollapse">
                        <table class="table table-user-information">
                            <tbody>
                                <tr>
                                    <td>Birthday:</td>
                                    <td><c:out value="${foundedPage.birthday}"/></td>
                                </tr>
                                <tr>
                                    <td>Home address:</td>
                                    <td><c:out value="${foundedPage.homeAddress}"/></td>
                                </tr>
                                <tr>
                                    <td>Work address</td>
                                    <td><c:out value="${foundedPage.workAddress}"/></td>
                                </tr>
                                <tr>
                                    <td>Skype:</td>
                                    <td><c:out value="${foundedPage.skype}"/></td>
                                </tr>
                                <tr>
                                    <td>ICQ:</td>
                                    <td><c:out value="${foundedPage.icq}"/></td>
                                </tr>
                                <c:forEach var="item" items="${foundedPage.phones}">
                                    <tr>
                                        <c:if test="${not empty item.phoneNumber}">
                                            <td>Phone:</td>
                                            <td><c:out value="${item.phoneNumber}"/></td>
                                        </c:if>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <br>
            <div id="wallPosts">
                <!-- Wall Posts In JS ---->
            </div>
        </div>
    </body>
</html>

