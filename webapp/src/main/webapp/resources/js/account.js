var limitFriendsLinkOnPage = 5;


$(document).ready(function () {

    var contextPath = $("#contextPath").html();

    /* -------- Get friends for page --------  */

    $.ajax({
        url:"/account/restFriends/",
        type: "get",
        cache: false
    }).done(function (data) {
        var friends = "";
        $.map(data, function (account) {
            friends = friends + "<div class=\"pull-left\">";
            friends = friends + "<a href=\"" + contextPath + "/id" + account.accountId + "\">";
            friends = friends + "<img class=\"media-object img-circle\" " +
                "src=\"data:image/jpeg;base64," + account.photo + "\" width=\"75px\"" +
                " height=\"75px\" style=\"margin-right:8px; margin-top:-5px;\">";
            friends = friends + "</a>";
            friends = friends + "</div>";
            friends = friends + "<h4>";
            friends = friends + "<a href = \"" + contextPath + "/id" + account.accountId + "\""
                + " style= \"text-decoration:none;\">" + "<strong>" + account.firstName + " " + account.secondName + " </strong></a>";
            friends = friends + "</h4>";
            friends = friends + "<br>";
            friends = friends + "<hr>";
            friends = friends + "<br>";
        });
        $("#accountFriends").html(friends);

    });

    /* -------- Get wall posts --------  */
    $.ajax({
        url: "/account/wallPosts/",
        type: "get",
        cache: false
    }).done(function (data) {
        $.map(data, function (wallMsg) {
            var showRemoveButton = true;
            $("#wallPosts").append(formatWallPost(wallMsg, contextPath, showRemoveButton));
        })
    });


    /* -------- new  wall post --------  */

    $("#newWallMessage").submit(function (e) {
        var pageId = $("#accountId").html();
        $.ajax({
            url: "/account/wallPosts/",
            type: 'post',
            data: {text: $("#newWallMessageInput").val(), ownerId: pageId},
            cache: false
        }).done(function (res) {
            if (res != null) {
                $("#wallPosts").prepend(formatWallPost(res, contextPath));
                $("#newWallMessageInput").val('');
            }
        });
        e.preventDefault();
    });


    $(".deletePostBtn").on("click", function (e) {
        removePost(this);
        e.preventDefault();
    });
});

function removePost(btn) {
    var id = $(btn).attr('id');
    var divIdToDelete = $('#' + 'wallPostId' + id);
    $.ajax({
        url: "/account/wallPosts/" + id,
        type: 'delete',
        cache: false
    }).done(function () {
        divIdToDelete.remove();
    });

}