/**
 * Created by mac on 21/12/17.
 */
/**
 * Created by mac on 26/10/17.
 */
$(document).ready(function () {
    $("#buttonParent").on("click", ".friendButton", function () {
        var buttonClass = $(".friendButton").attr("id");
        var pageId = $(".pageId").attr("id");
        if (buttonClass == 'notFriend') {
            $.ajax({
                method: "GET",
                url: "/account/newFriend",
                data: {"id": pageId},
                cache: false,
                success: function (result) {
                    var button = $(".friendButton");
                    if (result == 0) {
                        button.html('Friend request sent');
                        button.attr('id', 'wantFriend');
                    } else if (result == 2) {
                        button.html('remove from friends');
                        button.attr('id', 'nowFriend');
                        button.removeClass('btn btn-primary friendButton').addClass('btn btn-danger friendButton');
                    }
                }
            });
        } else if (buttonClass == "wantToFriend") {
            return false;
        } else {
            $.ajax({
                method: "GET",
                url: "/account/removeFriend",
                data: {"id": pageId},
                cache: false,
                success: function (result) {
                    var button = $(".friendButton");
                    if (result == 0) {
                        button.html('Add to friends');
                        button.attr('id', 'notFriend');
                        button.removeClass('btn btn-danger friendButton').addClass('btn btn-primary friendButton');
                    }
                }
            });
        }

    });


    $('.customCollapse').on('shown.bs.collapse', function () {
        $("#showInfoButton").text("Collapse");

    });

    $('.customCollapse').on('hidden.bs.collapse', function () {
        $("#showInfoButton").text("Show full info");
    });
});