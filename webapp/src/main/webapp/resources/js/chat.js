/**
 * Created by mac on 24/10/17.
 */
var contextPath = null;
var accountFriend = null;
var idAccount = null;
var idFriend = null;
var accountFromJsp = null;
var stompClient = null;
var chatData = null;


$(document).ready(function () {
    contextPath= $("#contextPath").html();
    idAccount = $("#accountId").html();

    $.ajax({
        method: "GET",
        url: "/account/restFriends/",
        data: {limit: 12},
        cache: false
    }).done(function (data) {
        var friends = "";
        $.map(data, function (account) {
            friends = friends + "<div onclick='initChat(" + account.accountId + ")'>";
            friends = friends + "<div class=\"pull-left\">";
            friends = friends + "<img class=\"media-object img-circle\" " +
                "src=\"data:image/jpeg;base64," + account.photo + "\" width=\"75px\"" +
                " height=\"75px\" style=\"margin-right:8px; margin-top:-5px;\">";
            friends = friends + "</div>";
            friends = friends + "<h4 >";
            friends = friends + "<strong>" + account.firstName + " " + account.secondName + " </strong>";
            friends = friends + "</h4>";
            friends = friends + "</div>";
            friends = friends + "<br>";
            friends = friends + "<hr>";
            friends = friends + "<br>";

        });
        $("#messagesWithFriend").html(friends);

    });

    if (idFriend != null) {
        initChat(idFriend);
    }

    $(".mytext").on("keyup", function (e) {
        if (e.which == 13) {
            sendMessage($(this).val(), idFriend);

        }
    });

    $("#sendMsg").on("click", function () {
        sendMessage($('.mytext').val(), idFriend);
    });

});



function initChat(friendId) {
    idFriend = friendId;

    console.log("init chat " + idFriend);

    initChatSession();

    connectToChat();

    loadChat();


};


function initChatSession() {
    console.log("idAccount: " + idAccount + "id Friend: " + idFriend);
    $.ajax({
        type: "post",
        url: "/account/getChatChannel",
        data: {userOneId: idAccount, userTwoId: idFriend},
        cache: false
       /* contentType: "application/json", // this
        dataType: "json"*/
    }).done(function (res) {
        chatData = res;
        setAccounts();
        console.log("Сhannel received with uuid" + res.channelUuid);
        console.log("Сhat data" + chatData.firstAccount.firstName);
        console.log("Сhat data" + chatData.firstAccount.secondName);
    });
};

function connectToChat() {

    var socket = new SockJS('/chatWebSocket');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function (frame) {
        console.log('Connected: ' + frame);
        stompClient.subscribe('/topic/chat.' + chatData.channelUuid, function (message) {
            addMessage( JSON.parse(message.body));
        });
    });

};

function loadChat() {
    $(".messageText").empty();
    $.ajax({
        type: "post",
        url: "/account/getMessages",
        data: {idFriend: idFriend},
        cache: false
    }).done(function (data) {
        $.map(data, function (message) {
            addMessage(message);
        });

        scrollToLast();
    });
};





function setAccounts() {
    if (chatData.firstAccount.accountId == idFriend){
        accountFriend = chatData.firstAccount;
        accountFromJsp = chatData.secondAccount;
    } else {
        accountFriend = chatData.secondAccount;
        accountFromJsp = chatData.firstAccount;
    }

};

function addMessage(message) {
    if (message.senderId != idFriend) {
        insertChat(message.text, "me", message.timeOfSend,
            accountFromJsp.firstName, accountFromJsp.photo);
    } else {
        insertChat(message.text, "notMe", message.timeOfSend,
            accountFriend.firstName, accountFriend.photo);
    }
   scrollToLast();
}

function insertChat(text, who, date, senderName, photo) {
    var control;
    var date = formatAMPM(date);

    if (who != "me") {

        control = '<li style="width:100%">' +
            '<div class="msj macro">' +
            '<div class="avatar"><img class="img-circle" style="width:40%;" src=\"data:image/jpeg;base64,' + photo + '" />' +
            '<p>' + senderName + '</p></div>' +
            '<div class="text text-l">' +
            '<p>' + text + '</p>' +
            '<p><small>' + date + '</small></p>' +
            '</div>' +
            '</div>' +
            '</li>';
    } else {
        control = '<li style="width:100%;">' +
            '<div class="msj-rta macro">' +
            '<div class="text text-r">' +
            '<p>' + text + '</p>' +
            '<p><small>' + date + '</small></p>' +
            '</div>' +
            '<div class="avatar" style="padding:0px 0px 0px 10px !important"><img class="img-circle" style="width:40%;" src=\"data:image/jpeg;base64,' + photo + '" />' +
            '<p>' + senderName + '</p></div>' +
            '</li>';
    }
    $(".messageText").append(control);
};


function formatAMPM(date1) {
    var date = new Date(date1);
    console.log(date);
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0' + minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
};


function sendMessage(message, idFriend) {
    var idAccount = $("#accountId").html();
    stompClient.send("/appWs/chat.sendMsg." + chatData.channelUuid, {}, JSON.stringify({
        'text': message,
        'recipientId': idFriend,
        'senderId': idAccount
    }));

};

function scrollToLast() {
    $('.messageText').animate({scrollTop: $('.messageText').prop("scrollHeight")}, 5);
}

/*
function send(message) {
    if (message != "") {
        $.ajax({
            method: "GET",
            url: "/account/sendMsg",
            data: {msg: message, id: $('#idFriend').html()},
            cache: false
        }).done (function(res){
           if (res == 0) {
               insertChat(message, "me", new Date(), $('#accountName').html(), $('#photoAccount').html());
           }
        });
        $(".mytext").val('');
        $('.messageText').animate({scrollTop: $('.messageText').prop("scrollHeight")}, 5);
    }

}*/
