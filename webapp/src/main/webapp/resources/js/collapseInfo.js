/**
 * Created by mac on 21/12/17.
 */
$(document).ready(function () {

    $('.customCollapse').on('shown.bs.collapse', function () {
        $("#showInfoButton").text("Collapse");

    });

    $('.customCollapse').on('hidden.bs.collapse', function () {
        $("#showInfoButton").text("Show full info");
    });
})