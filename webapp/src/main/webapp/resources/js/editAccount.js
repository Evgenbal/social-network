

$(document).ready(function () {
    var contextPath = $("#contextPath").html();
    $(function () {

        var hash = document.location.hash;
        var prefix = "tab_";
        if (hash) {
            $('.nav-tabs a[href="' + hash.replace(prefix, "") + '"]').tab('show');
        }
    });

    $('.nav-tabs a').on('shown', function (e) {
        window.location.hash = e.target.hash.replace("#", "#" + prefix);
    });

    $('#confirmReceived').on("click", function () {
        $("#submitChanges").trigger("click");
    });


    $(".phoneMask").mask("+7 (999) 999-99-99");

    $(".birthdayMask").mask("9999-99-99");

    $("#revertMail").on("click", function (e) {
        e.preventDefault();
        $("#editedMail").val($("#editedMail").data("default-value"));
    });


    var count = $('#count').html();
    var user = $('#userName').html();

    $('.removePhone').on("click", function () {
        var n = $(this).attr("id");
        $('#phone' + n).remove();
    });

    $('#addRow').on("click", function () {
        var div = document.createElement('div');

        div.setAttribute('class', "form-group");
        div.innerHTML = '<label class="col-md-3 control-label">Phone number:</label>\
                    <div class="col-md-7">\
                    \
                         <input class="form-control phoneMask" type="text" name="phones[' + count + '].phoneNumber">\
                         \
                         <input type="hidden" value="' + user + '" name="phones[' + count + '].user.accountId">\
                    </div>\
                    <div class="col-md-1">\
                        <input type="button" value="-" onclick="removeRow(this)">\
                    </div>';
        document.getElementById('fieldOfPhones').appendChild(div);
        count++;

        $('.phoneMask').mask('+7 (999) 999-99-99');

    });


    $('#formMainInfo').submit(function (e) {
        var form = $('#formMainInfo')[0];
        var fdata = new FormData(form);
        $.ajax({
            type: 'POST',
            url: contextPath + "/account/editMainInfo",
            data: fdata,
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false
        }).done(function (res) {
            if (res.validated) {
                showMsg("Your profile has been successfully updated.", true);

            } else {
                $.each(res.errorMessages, function (key, value) {
                    $('input[name=' + key + ']').after('' +
                        '<div class="alert alert-danger">' +
                        '<span class="glyphicon glyphicon-remove"> ' + value + '</span>' +
                        '</div>'
                    )
                    ;
                });
            }

        });
        e.preventDefault();
    });

    $('#saveChangesSecurity').on("click", function (e) {
        var newPassword = $("#newPassword").val();
        var controlPassword = $("#controlPassword").val();
        var oldPassword = $("#oldPassword").val();
        var mail = $("#editedMail").val();
        var str = "";
        if (newPassword != controlPassword) {
            str = "Entered passwords do not match";
        }
        if (oldPassword == '') {
            str = "Please enter current password";
        }
        if (mail == '') {
            str = "Mail can not be empty";
        } else if (!validateEmail(mail)) {
            str = "Wrong mail format"
        }
        if (newPassword==oldPassword) {
            str = "New password and current password are same. Update does not need"
        }
        if (str != "") {
            showMsg(str, false)
        } else {
            $.ajax({
                type: "post",
                url: contextPath + "/account/editSecurity",
                data: {newPassword: newPassword, oldPassword: oldPassword, mail: mail},
                cache: false
            }).done(function (status) {
                var str;
                var msgStatusOk;
                switch (status) {
                    case 0:
                        str = "Update is not required.";
                        msgStatusOk = true;
                        break;
                    case 1:
                        str = "Wrong current password.";
                        msgStatusOk = false;
                        break;
                    case 2:
                        str = "User with entered email already exist.";
                        msgStatusOk = false;
                        break;
                    case 3:
                        str = "Your profile has been successfully updated."
                        msgStatusOk = true;
                        break;
                }
                showMsg(str, msgStatusOk);
            });
        }
        e.preventDefault()
    });

    $('#loadAccountFromXml').submit(function (e) {
        var form = $('#loadAccountFromXml')[0];
        var fdata = new FormData(form);
        $.ajax({
            type: 'POST',
            url: contextPath + "/account/loadFromXml",
            data: fdata,
            processData: false,
            contentType: false
        }).done(function (res) {
            if (res==0) {
                showMsg("Account updated from XML file", true);
                $('#changeXml').modal('hide');
            }
        });
        e.preventDefault()
    })
});


function removeRow(element) {
    element.parentNode.parentNode.parentNode.removeChild(element.parentNode.parentNode);
}

function hideWithTime(clazz) {
    $(clazz).delay(2000).fadeOut();
}

function validateEmail(email) {
    var re = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;
    return re.test(String(email).toLowerCase());
}

function showMsg(str, status) {
    var divClass;
    var spanClass;
    if (status) {
        divClass = "alert alert-success";
        spanClass = "glyphicon glyphicon-ok-circle";
    } else {
        divClass = "alert alert-danger";
        spanClass = "glyphicon glyphicon-remove";
    }
    var div = document.createElement("div");
    div.setAttribute("class", divClass);
    div.innerHTML = "<span class=\"" + spanClass + "\"></span> " +
        str;
    $("#infoMessage").append(div);
    hideWithTime(div);

}
