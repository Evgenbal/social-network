function formatWallPost(post, contextPath, buttonRemove) {
    var buttonStr = ""
    if (buttonRemove) {
        buttonStr = ' <button class="glyphicon glyphicon-remove " type="button"' +
            ' id="' + post.messageId + '" onclick="removePost(this)">';
    }
    var str =
        '<div class="panel panel-default" id="wallPostId' + post.messageId + '">' +
        '<div class="panel-body">' +
        ' <div class="pull-left"> ' +
        '<a href="' + contextPath + '/id' + post.senderId + '">' +
        '<img class="media-object img-circle" src="data:image/jpeg;base64,' + post.senderPhoto + '"' +
        ' width="50px"' +
        ' height="50px" style="margin-right:8px; margin-top:-5px;">' +
        '</a>' +
        ' </div>' +
        ' <h4>' +
        ' <a href="' + contextPath + '/id' + post.senderId + '" style="text-decoration:none;">' +
        ' <strong>' + post.senderFirstName + ' ' + post.senderSecondName + '</strong></a> –' +
        '<small>' +
        '<a href="' + contextPath + '/id' + post.senderId + '"' +
        '  style="text-decoration:none; color:grey;"><i><i class="fa fa-clock-o"' +
        '                                aria-hidden="true">' +
        '  </i> ' + new Date().toISOString() + '</i></a>' +
        ' </small>' +
        '</h4>' +
        '<div class="navbar-right">' +
        ' <div class="dropdown">' + buttonStr +
        '  </button>' +
        ' </div>' +
        '</div>' +
        '<hr>' +
        ' <div class="post-content">' +
        post.text +
        ' </div>' +
        ' <hr> ' +
        '</div>' +
        '</div>';
    return str;
}