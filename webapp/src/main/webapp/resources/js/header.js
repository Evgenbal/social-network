$(document).ready(function () {
    var contextPath = $("#contextPath").html();
    $('#filterInHeader').autocomplete({
        source: function (request, response) {
            $.ajax({
                url: contextPath + "/account/searchProgress",
                data: {
                    filter: request.term
                },
                method: "GET",
                success: function (data) {
                    var viewAll = $.makeArray({
                        value: "view all results",
                        url: contextPath + "/account/allResults?filter=" + request.term,
                        label: "All results"
                    });
                    var accounts = $.map(data, function (account) {
                        return {
                            value: account.firstName,
                            url: "/webapp-1.0-SNAPSHOT/id" + account.accountId,
                            label: account.firstName + ' ' + account.secondName,
                            imgsrc: account.photo
                        };
                    });
                    response($.merge(viewAll, accounts));
                }

            });

        },
        select: function (event, ui) {
            window.location = ui.item.url;
            return false;
        },
        minLength: 2,

    }).data("ui-autocomplete")._renderItem = function (ul, item) {
        if (item.imgsrc != null) {
            return $("<li></li>")
                .data("ui-autocomplete-item", item)
                .append("<a>" + "<img class='img-circle' style='width:25px;height:25px' src= data:image/png;base64," + item.imgsrc + " /> " + item.label + "</a>")
                .appendTo(ul);
        } else {
            return $("<li></li>")
                .data("ui-autocomplete-item", item)
                .append("<a>" + "<span class=\"glyphicon glyphicon-search\"></span>" + item.label + "</a>")
                .appendTo(ul);
        }

    };
});

