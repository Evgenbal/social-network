$(document).ready(function ($) {
    $("#username").keyup(function(event){
        if(event.keyCode == 13){
            $("#login-submit").click();
        }
    });
    $("#password").keyup(function(event){
        if(event.keyCode == 13){
            $("#login-submit").click();
        }
    });
    $('.msg').delay(2000).fadeOut();
    $('.error').delay(2000).fadeOut();
    $(window).keydown(function(event){
        if(event.keyCode == 13) {
            event.preventDefault();
            $("#login-submit").trigger("click");
            return false;
        }
    });
    $("#login-submit").on("click", function () {
        var user = $("#mail");
        var password = $('#password');
        /*if (user !== null && user.val() !== "" && password !== null && password.val() !== "") {

        }*/
        if (user === null || user.val() === "") {
                user.animate({backgroundColor: "#faeaea"}, 500).animate({backgroundColor: "#fff"}, 500);

        } else if (password === null || password.val() === "") {
                password.animate({backgroundColor: "#faeaea"}, 500).animate({backgroundColor: "#fff"}, 500);

        } else {
            $("#login-submit-forJS").trigger("click");
        }
    });
});
