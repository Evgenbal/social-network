

$(document).ready(function () {
    $("#registrationForm").submit(function (e) {
        var mail = $("#mail").val();
        var mainPassword = $("#password").val();
        var passwordTest = $("#passwordTest").val();
        var str = "";
        var status = true;
        if (password = "") {
            str = "Please enter password";
            status = false;
        }

        if (mail == '') {
            str = "Mail can not be empty";
            status = false;
        } else if (!validateEmail(mail)) {
            str = "Wrong mail format";
            status = false;
        }
        if (mainPassword != passwordTest) {
            str = "Entered passwords do not match";
            status = false;
        }

        if (!status){
            showMsg(str, status);
            e.preventDefault();
        }

    });
});

function hideWithTime(clazz) {
    $(clazz).delay(2000).fadeOut();
}

function validateEmail(email) {
    var re = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;
    return re.test(String(email).toLowerCase());
}

function showMsg(str, status) {
    var divClass;
    var spanClass;
    if (status) {
        divClass = "alert alert-success";
        spanClass = "glyphicon glyphicon-ok-circle";
    } else {
        divClass = "alert alert-danger";
        spanClass = "glyphicon glyphicon-remove";

    }
    var div = document.createElement("div");
    div.setAttribute("class", divClass);
    div.innerHTML = "<span class=\"" + spanClass + "\"></span> " +
        str;
    $("#infoMessage").append(div);
    hideWithTime(div);

}