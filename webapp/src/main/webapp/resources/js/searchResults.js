
$(document).ready(function () {
    var countOfEntries;
    var countOfPages;
    var currentPageNumber = 1;
    var entriesPerPage = 2;
    var countOfEntriesToRead = entriesPerPage;
    var listOfAccounts = $('#accountList');
    var filter;
    var contextPath = $("#contextPath").html();

    if($('#filter').val()){
        initSearch();
    }

    $("#filter").keyup(function () {
        initSearch();
    });

    $(".page").on("click", function () {
        currentPageNumber = Number($(this).html());
        setPagination(currentPageNumber);
        initRead();

    });

    function initSearch() {
        filter = $("#filter").val();
        if (filter.length > 2) {
            listOfAccounts.empty();
            $.ajax({
                method: "GET",
                url: contextPath + "/account/countOfSearch",
                data: {"filter": filter},
            }).done(function (data) {
                countOfEntries = Number(data);

                countOfPages = Math.floor(countOfEntries / entriesPerPage);

                if (countOfEntries % entriesPerPage != 0) {
                    countOfPages++;
                }
                currentPageNumber = 1;
                setPagination(currentPageNumber);
                $("#last").html(countOfPages);
                initRead();

            });
        }
    }

    function initRead() {
        var lastRead = Number(entriesPerPage * currentPageNumber);
        if (currentPageNumber == countOfPages) {
            countOfEntriesToRead = countOfEntries % entriesPerPage;
            lastRead = countOfEntries;
        }
        if (countOfEntriesToRead == 0) {
            countOfEntriesToRead = entriesPerPage;
        }
        getEntries(currentPageNumber, entriesPerPage, filter);
    }

    function setPagination(currentPageNumber) {
        $("#current").show().html(currentPageNumber);
        var first = $("#first");
        var passAtBegin = $("#passAtBegin");
        var currentMinusOne = $("#currentMinusOne");
        var currentPlusOne = $("#currentPlusOne");
        var passInEnd = $("#passInEnd");
        var last = $('#last');

        setNextPages(currentPageNumber, currentPlusOne, passInEnd, last);
        setPrevPages(currentPageNumber, currentMinusOne,passAtBegin, first);
    }

    function setNextPages(currentPageNumber, currentPlusOne, passInEnd, last) {
        if (currentPageNumber == countOfPages) {
            $(currentPlusOne).hide();
            $(passInEnd).hide();
            $(last).hide();
        } else if (currentPageNumber == countOfPages - 1) {
            $(last).show();
            $(passInEnd).hide();
            $(currentPlusOne).hide();
        } else if (currentPageNumber == countOfPages - 2) {
            $(last).show();
            $(passInEnd).hide();
            $(currentPlusOne).show();
            $(currentPlusOne).html(currentPageNumber + 1);
        } else {
            $(last).show();
            $(passInEnd).show();
            $(currentPlusOne).show();
            $(currentPlusOne).html(currentPageNumber + 1);
        }
    }

    function setPrevPages(currentPageNumber, currentMinusOne, passAtBegin, first) {
        if (currentPageNumber == 1) {
            $(first).hide();
            $(passAtBegin).hide();
            $(currentMinusOne).hide();
        } else if (currentPageNumber == 2) {
            $(first).show();
            $(passAtBegin).hide();
            $(currentMinusOne).hide();
        } else if (currentPageNumber == 3) {
            $(first).show();
            $(passAtBegin).hide();
            $(currentMinusOne).show();
            $(currentMinusOne).html(currentPageNumber - 1);
        } else {
            $(first).show();
            $(passAtBegin).show();
            $(currentMinusOne).show();
            $(currentMinusOne).html(currentPageNumber - 1);
        }
    }

    function getEntries(page, size, filter) {
        $.ajax({
            method: "GET",
            url: contextPath + "/account/searchProgress",
            data: {"filter": filter, "page": page, "size": size},
            cache: false,
            success: function (data) {
                var listAccount = "";
                $.map(data, function (account) {
                    listAccount = listAccount + "<div class=\"pull-left\">";
                    listAccount = listAccount + "<a href=\"" + $("#contextPath").html() + "/id" + account.accountId + "\">";
                    listAccount = listAccount + "<img class=\"media-object img-circle\" " +
                        "src=\"data:image/jpeg;base64," + account.photo + "\" width=\"75px\"" +
                        " height=\"75px\" style=\"margin-right:8px; margin-top:-5px;\">";
                    listAccount = listAccount + "</a>";
                    listAccount = listAccount + "</div>";
                    listAccount = listAccount + "<h4>";
                    listAccount = listAccount + "<a href = \"" + $("#contextPath").html() + "/id" + account.accountId + "\"" + " style= \"text-decoration:none;\">" + "<strong>" + account.firstName + " " + account.secondName + " </strong></a>";
                    listAccount = listAccount + "</h4>";
                    listAccount = listAccount + "<br>";
                    listAccount = listAccount + "<hr>";
                    listAccount = listAccount + "<br>";

                });
                listOfAccounts.html(listAccount);
            }
        });
    }
});