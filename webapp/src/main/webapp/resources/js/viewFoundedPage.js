var friendId = null;
$(document).ready(function () {

    var contextPath = $("#contextPath").html();
    friendId = $("#accountId").html();

    /* -------- Get wall posts --------  */
    $.ajax({
        url: "/account/wallPosts/" + friendId,
        type: "get",
        cache: false
    }).done(function (data) {
        $.map(data, function (wallMsg) {
            var showButtonToRemove = false;
            $("#wallPosts").append(formatWallPost(wallMsg, contextPath, showButtonToRemove));
        })
    });

    /* -------- Get friends for page --------  */
    $.ajax({
        url: "/account/restFriends/" + friendId,
        type: "get",
        cache: false
    }).done(function (data) {
        var friends = "";
        $.map(data, function (account) {
            friends = friends + "<div class=\"pull-left\">";
            friends = friends + "<a href=\"" + contextPath + "/id" + account.accountId + "\">";
            friends = friends + "<img class=\"media-object img-circle\" " +
                "src=\"data:image/jpeg;base64," + account.photo + "\" width=\"75px\"" +
                " height=\"75px\" style=\"margin-right:8px; margin-top:-5px;\">";
            friends = friends + "</a>";
            friends = friends + "</div>";
            friends = friends + "<h4>";
            friends = friends + "<a href = \"" + contextPath + "/id" + account.accountId + "\""
                + " style= \"text-decoration:none;\">" + "<strong>" + account.firstName + " " + account.secondName + " </strong></a>";
            friends = friends + "</h4>";
            friends = friends + "<br>";
            friends = friends + "<hr>";
            friends = friends + "<br>";
        });
        $("#accountFriends").html(friends);
    });


    /* -------- Get status with account in session --------  */

    getStatus();


});

function getStatus() {
    $.ajax({
        url: "/account/restFriends/status/" + friendId,
        type: "get",
        cache: false
    }).done(function (data) {
        updateFriendButton(data);

    });

};

function updateFriendShip(data) {
    var method = $(data).attr("id");
    $.ajax({
        type: method,
        url: "/account/restFriends/" + friendId,
        cache: false
    }).done(function (data) {
        $("#buttonParent").html(data);
       // updateFriendButton(data);
    });
};

function updateFriendButton(status) {
    var button = "";
    switch (status.toLowerCase()) {
        case "friend":
            button = '<button type="button" id="delete" class="btn btn-warning " onclick="updateFriendShip(this)">' +
                '                             Remove from friendlist' +
                '                         </button>';

            break;
        case "not_friend":
            button = '<button type="button" id="put" class="btn btn-primary " onclick="updateFriendShip(this)">' +
                '                             Add to friends' +
                '                         </button>';
            break;
        case "wait_for_accept":
            button =
                '<div class="btn-group"> ' +
                '<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">' +
                'You are subscribed' +
                '                             <span class="caret"></span>' +
                '                         </button>' +
                '                         <ul class="dropdown-menu">' +
                '                             <li><a href="" id="delete" onclick="updateFriendShip(this)">Unsubscribe</a></li>' +
                '                         </ul>' +
                '</div>';
            break;
    }
    $("#buttonParent").html(button);

};

